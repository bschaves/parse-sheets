#!/usr/bin/env python3
#

"""
Configurações básicas:
- instalar o idioma pt-br
    Linux -> sudo apt-get install tesseract-ocr-por

    Windows -> Baixe o arquivo por.traineddata (https://github.com/tesseract-ocr/tessdata)

- Extraia o texto da imagem usando o idioma pt-BR 
        texto = pytesseract.image_to_string(imagem, lang='por')

- Defina o Caminho para o Executável do Tesseract
    pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'

- Defina o diretório onde os arquivos de linguagem estão localizados 
    os.environ['TESSDATA_PREFIX'] = r'C:\\caminho\\para\\o\\diretorio\\das\\linguagens'

"""

import sys
import os

this_script = os.path.abspath(__file__)
this_dir = os.path.dirname(this_script)

sys.path.insert(0, this_dir)
from gui.gui_master import run_gui_ocr

def main():
    run_gui_ocr()
    

if __name__ == '__main__':
    main()



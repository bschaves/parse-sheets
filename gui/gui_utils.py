#!/usr/bin/env python3
#
from __future__ import annotations
from typing import List, Callable
from pathlib import Path
from typing import (Dict, List)

import threading
import tkinter as tk
from tkinter import ( 
        filedialog, 
        messagebox,
        Tk,
        ttk,
)

from tkinter.ttk import (
        Frame,
)

from doc_convert import (
    File,
    Directory,
    get_abspath,
)

from doc_convert.info.info_ocr import (
    UserPrefs,
    OcrAppDirs,
)

def show_warnnings(text:str):
    messagebox.showwarning("Aviso", text)

class SharedValues(object):
    def __init__(self) -> None:
        self.initialDir:str = get_abspath(Path().home())
        self.tesseract:str = None
        self.ocrmypdf:str = None
        self.lastDir:str = get_abspath(Path().home())

class GetWidgets(object):
    def __init__(self, root) -> None:
        self.root = root
        self.pady = 2
        self.padx = 3
        self.height = 2
        self.backGroundBlack = "#333333"
        self.backGround = self.backGroundBlack

        self.fontArial = ('Arial', 11)
        self.font = self.fontArial
        self.fontButtons = ('Arial', 12)
        # Estilo para os Frames
        self.styleFrame = ttk.Style()
        self.styleFrame.theme_use("default")
        self.styleFrame.configure(
                            "Custom.TFrame", 
                            background="#2C2C2C"
                        )  # Cor de fundo personalizada

        # Estilo para os botões
        self.styleButtonGreen = ttk.Style()
        self.styleButtonGreen.theme_use("default")
        self.styleButtonGreen.configure(
                "TButton",
                foreground="white",
                background="#4CAF50",  # Verde padrão
                font=("Helvetica", 12)
            )
        
        self.styleButtonGreen.map(
            'TButton',             # Nome do estilo
            background=[('active', 'darkblue')],  # Cor de fundo ao passar o mouse
            foreground=[('disabled', 'gray')]    # Cor do texto quando desabilitado
        )
        
        self.style_btn_blue = ttk.Style()
        self.style_btn_blue.configure(
            "Custom.TButton",         # Nome do estilo
            font=("Helvetica", 14),   # Fonte personalizada
            foreground="white",       # Cor do texto
            background="blue"         # Cor de fundo
        )

    def void_command(self) -> None:
        print(f'VOID')

    def get_progress_bar(self, parent:ttk.Frame=None, *, mode="indeterminate") -> ttk.Progressbar:
        if parent is None:
            parent = self.root
        return ttk.Progressbar(
                            parent, 
                            orient="horizontal", 
                            length=320, 
                            mode=mode,
                        )

    def frame(self, parent:ttk.Frame=None) -> ttk.Frame:
        if parent is None:
            parent = self.root
        return ttk.Frame(parent, relief="groove")
    
    def get_label(self, parent:ttk.Frame=None) -> ttk.Label:
        if parent is None:
            parent = self.root
        return ttk.Label(
                    parent, 
                    text="-"
                )

    def get_button(self, parent:ttk.Frame=None) -> ttk.Button:
        if parent is None:
            parent = self.root

        return ttk.Button(
                    parent, 
                    text="botão", 
                    command=self.void_command,
                    style='TButton',
                )  

    def get_input_box(self, parent:ttk.Frame=None) -> ttk.Entry:
        if parent is None:
            parent = self.root
        return ttk.Entry(parent)     

    def void_label(self, parent:ttk.Frame=None) -> ttk.Label:
        if parent is None:
            parent = self.root    
        lb = ttk.Label(parent)
        lb.config(padding=2)
        return lb

    def get_combobox(self, parent:ttk.Frame=None, values:List[str]=['-']) -> ttk.Combobox:
        if parent is None:
            parent = self.root
        return ttk.Combobox(parent, values=values)

    def get_radio_button(self, parent=None) -> ttk.Radiobutton:
        if parent is None:
            parent = self.root
        return ttk.Radiobutton(
                    parent, 
                    text="Opções", 
                )
    
class OpenFiles(object):
    def __init__(
                self, 
                appdir:OcrAppDirs, 
                *, 
                sharedValues:SharedValues=SharedValues()
            ) -> None:
        self.appdir:OcrAppDirs = appdir
        # Valores compartilhados entre as janelas
        self.sharedValues:SharedValues = sharedValues

    def open_filename(self) -> str | None:
        """
            Caixa de dialogo para selecionar um arquivo
        """
        filename = filedialog.askopenfilename(
                                    title="Selecione um arquivo",
                                    initialdir=self.appdir.ufs.dir_downloads.absolute(),
                                    filetypes=[
                                        ("Todos os arquivos", "*"),
                                    ]
                                )
        if filename:
            return filename
        return None

    def open_file_sheet(self) -> str | None:
        """
            Caixa de dialogo para selecionar um arquivo CSV/TXT/XLSX
        """
        filename = filedialog.askopenfilename(
                                    title="Selecione um arquivo",
                                    initialdir=self.appdir.ufs.dir_downloads.absolute(),
                                    filetypes=[
                                        ("Arquivos Excel", "*.xlsx *.xls"),
                                        ("Arquivos CSV", "*.csv *.txt"),
                                    ]
                                )
        if filename:
            return filename
        return None

    def open_files_sheet(self) -> list:
        """
            Selecionar uma ou mais planilhas

        """
        files = filedialog.askopenfilenames(
                    title='Selecionar planilhas',
                    initialdir=self.appdir.ufs.dir_downloads.absolute(),
                    filetypes=[
                        ("Arquivos Excel", "*.xlsx *.xls"),
                        ("Arquivos CSV", "*.csv *.txt"),
                    ]
                )
        return files


    def open_folder(self) -> str | None:
        d = filedialog.askdirectory(
                                    initialdir=self.sharedValues.initialDir,
                                )
        if d is None:
            return None
        self.sharedValues.lastDir = d
        return d
    
class ControllerApp(Tk):
    def __init__(self, *, appDirs:OcrAppDirs, sharedValues:SharedValues):
        super().__init__()
        self.appDirs:OcrAppDirs = appDirs
        self.sharedValues:SharedValues = sharedValues
        self.openFiles:OpenFiles = OpenFiles(self.appDirs, sharedValues=self.sharedValues)
        self.inputFiles:List[File] = []
        self.inputDir:Directory = None
        self.saveDir:Directory = self.appDirs.dirOutputFiles()
        self.user_prefs:UserPrefs = self.appDirs.default_prefs()
        self.last_frame:Frame = None
        self.pages:Dict[str, AppPage] = {}
        self.navigatorPages = Navigator(parent=self, controller=self)

    def to_page(self, page_name:str):
        self.last_frame.pack_forget()
        self.last_frame = self.pages[page_name]
        frame = self.pages[page_name]
        frame.set_size_screen()
        frame.update_state()
        frame.pack()
        
class AppPage(ttk.Frame):
    def __init__(self, *, parent:Tk, controller:ControllerApp):
        super().__init__(parent)
        self.parent:Tk = parent
        self.controller:ControllerApp = controller
        self.widgets:GetWidgets = GetWidgets(self.parent)
        self.frame_master = self.widgets.frame(self)
        self.frame_master.config(style="Custom.TFrame")
        self.frame_master.pack(expand=True, fill=tk.BOTH)
        self.page_name:str = None
        self.running_main_thread:bool = False
        self.list_stop_events:List[threading.Event] = []
        self.set_size_screen()

    def commandStopButton(self):
        """
            Esse método pode ser conectado a um botão para para a Thread principal.
        Podendo ser conectado diretamente ou indiretamente.
        """
        pass

    def add_button_back(self, container:Frame=None):
        if container is None:
            container = self.frame_master
        # botão voltar
        self.btn_back = self.widgets.get_button(container)
        self.btn_back.config(text='Voltar', command=self.go_back_page)
        self.btn_back.pack()

    def go_back_page(self):
        self.controller.navigatorPages.pop()
    
    def set_size_screen(self):
        pass

    def update_state(self):
        pass


class Navigator(object):
    def __init__(self, *, parent:Tk, controller:ControllerApp):
        self.parent:Tk = parent  # Janela principal ou root
        self.controller:ControllerApp = controller
        self.pages:Dict[str, AppPage] = {}  # Dicionário para armazenar as páginas
        self.current_page = None  # Página atualmente exibida
        self.history:List[str] = []  # Pilha para armazenar o histórico de navegação

    def add_page(self, page_class: AppPage):
        """
        Adiciona uma página ao navegador.

        :param page: Instância da página (AppPage).
        """
        p:AppPage = page_class(parent=self.parent, controller=self.controller)
        self.pages[p.page_name] = p
        print(f'Página adicionada: {p.page_name}')

    def push(self, page_name: str):
        """
        Exibe a página especificada.

        :param page_name: Nome da página a ser exibida.
        """
        print(f'Navegando para {page_name}')
        if page_name not in self.pages:
            show_warnnings(f'Página não encontrada!\n{page_name}')
            return 
            #raise ValueError(f"A página '{page_name}' não foi registrada no Navigator.")

        # Esconde a página atual, se houver
        if self.current_page is not None:
            self.history.append(self.current_page.page_name)  # Salva no histórico
            self.current_page.pack_forget()

        # Mostra a nova página
        self.current_page: AppPage = self.pages[page_name]
        self.current_page.set_size_screen()
        self.current_page.update_state()
        #self.current_page.pack(fill="both", expand=True)
        self.current_page.pack()
        
    def pop(self):
        """
        Retorna à página anterior no histórico de navegação.
        """
        if not self.history:
            raise ValueError("Não há páginas anteriores no histórico para retornar.")

        # Esconde a página atual
        if self.current_page is not None:
            self.current_page.pack_forget()

        # Recupera a página anterior do histórico
        previous_page_name = self.history.pop()
        self.current_page: AppPage = self.pages[previous_page_name]
        self.current_page.set_size_screen()
        self.current_page.update_state()
        #self.current_page.pack(fill="both", expand=True)
        self.current_page.pack()
        print(f'Retornando para {previous_page_name}')

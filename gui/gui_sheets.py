#!/usr/bin/env python3
#

import os
import time
import pandas
from typing import (List)
import threading
from threading import Thread, Event

from doc_convert import (
    File, 
    FileSheet,
    Directory, 
    InputFiles,
    InputDataSheet,
    DataFrame,
    save_dataframe,
    FormatData,
    KERNEL_TYPE,
)

from doc_convert.utils._version import (
        __version__, 
        __author__, 
        __update__,
        __version_lib__,
        __url__,
)

import tkinter as tk

from tkinter.ttk import (
    Progressbar, 
    Button,
    Entry, 
    Frame, 
    Label,
)

from gui.gui_utils import (
    show_warnnings,
    AppPage,
    ControllerApp,
)

#========================================================#
# Filtrar Planilha
#========================================================#
class PageSoupSheets(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.page_name = '/home/select_actions/soup_sheets'
        # Informações recebidas da janela anterior.
        self.selectedSheets: List[FileSheet] = []
        self.numFiles:int = len(self.selectedSheets)
        
        # Informações da janela atual
        self.sheetFilter:FileSheet = None
        self.format_data: FormatData = None
        self._main_dataframe:DataFrame = None
        self.mainThread: Thread = None
        self.running = False

        self.initUI()
        #self.operation_start_values()
        
    def initUI(self) -> None:
        self.listColumnNames: List[str] = ['-']

        #------------------------------------------------#
        # Frame para agrupar ações de Edição
        # Concatenar/Apagar
        #------------------------------------------------#
        self.frame_edit_actions:Frame = self.widgets.frame(self.frame_master)
        self.frame_edit_actions.pack(expand=True, padx=1, pady=1, fill=tk.BOTH)
        # Label informativo
        self.label_edit_info = self.widgets.get_label(self.frame_edit_actions)
        self.label_edit_info.config(text='Opções de Edição')
        self.label_edit_info.pack(expand=True, padx=1, pady=1)

        #---------------------------------------------------------------#
        # Frame widgets para apagar linhas
        #---------------------------------------------------------------#
        self.frame_remove_lines = self.widgets.frame(self.frame_edit_actions)
        self.frame_remove_lines.pack(expand=True, padx=1, pady=1, fill=tk.BOTH)
        # Combobox para apagar valore nulos de uma coluna qualquer
        self.label_remove_null = self.widgets.get_label(self.frame_remove_lines)
        self.label_remove_null.config(text='Apagar linhas vazias')
        self.label_remove_null.pack(side=tk.LEFT, expand=True, padx=1, pady=1)
        # Combobox ao lado do label APAGAR.
        self.combobox_yesno = self.widgets.get_combobox(self.frame_remove_lines, self.listColumnNames,)
        self.combobox_yesno.set('-')
        self.combobox_yesno.pack(side=tk.LEFT, expand=True, padx=1, pady=1)
        # Botão para apagar valores vazios da coluna selecionda.
        self.btn_ok = self.widgets.get_button(self.frame_remove_lines)
        self.btn_ok.config(text='Apagar', command=self.act_btn_remove_null)
        self.btn_ok.pack(side=tk.LEFT, expand=True, padx=1, pady=1)

        #---------------------------------------------------------------#
        # Frame Remover colunas ##########
        #---------------------------------------------------------------#
        self.frame_remove_columns:Frame = self.widgets.frame(self.frame_edit_actions)
        self.frame_remove_columns.pack(expand=True, padx=1, pady=1, fill=tk.BOTH)
        # Botão/Combobox para apagar colunas
        self.label_rm_col = self.widgets.get_label(self.frame_remove_columns)
        self.label_rm_col.config(text='Apagar coluna')
        self.label_rm_col.pack(side=tk.LEFT, expand=True, padx=1, pady=1)
        # Combobox ao lado do label.
        self.combo_remove_colums = self.widgets.get_combobox(self.frame_remove_columns, self.listColumnNames)
        self.combo_remove_colums.set('-')
        self.combo_remove_colums.pack(side=tk.LEFT, expand=True, padx=1, pady=1)
        # Botão de ação para remover colunas da planilha
        self.btn_remove_columns:Button = self.widgets.get_button(self.frame_remove_columns)
        self.btn_remove_columns.config(text='Apagar', command=self.act_btn_del_col)
        self.btn_remove_columns.pack(side=tk.LEFT, expand=True, padx=1, pady=1, fill=tk.BOTH)

        #---------------------------------------------------------------#
        # Frame para concatenar colunas
        #---------------------------------------------------------------#
        self.frame_concat = self.widgets.frame(self.frame_edit_actions)
        self.frame_concat.pack(expand=True, padx=1, pady=1, fill=tk.BOTH)
        # Combobox para concatenar colunas
        self.label_concat = self.widgets.get_label(self.frame_concat)
        self.label_concat.config(text='Concatenar')
        self.label_concat.pack(side=tk.LEFT, expand=True, padx=1, pady=1)
        # Combo 1 de 3
        self.combo_concat_1 = self.widgets.get_combobox(self.frame_concat, self.listColumnNames,)
        self.combo_concat_1.set('-')
        self.combo_concat_1.config(width=7)
        self.combo_concat_1.pack(side=tk.LEFT, padx=1, pady=1)
        # Combo 2 de 3
        self.combo_concat_2 = self.widgets.get_combobox(self.frame_concat, self.listColumnNames,)
        self.combo_concat_2.set('-')
        self.combo_concat_2.config(width=7)
        self.combo_concat_2.pack(side=tk.LEFT, padx=1, pady=1)
        # Combo 3 de 3
        self.combo_concat_3 = self.widgets.get_combobox(self.frame_concat, self.listColumnNames,)
        self.combo_concat_3.set('-')
        self.combo_concat_3.config(width=7)
        self.combo_concat_3.pack(side=tk.LEFT, padx=1, pady=1)
        # botão concatenar
        self.btn_concat = self.widgets.get_button(self.frame_concat)
        self.btn_concat.config(text='Concatenar', command=self.action_concat_columns,)
        self.btn_concat.pack(side=tk.LEFT, expand=True, padx=1, pady=1)

        #------------------------------------------------#
        # Frame para agrupar a coluna filtro, o texto a ser filtrado e a planilha de filtro.
        #------------------------------------------------#
        # Frame para agrupar os widgets de filtro e exportação de dados.
        self.frame_filter_and_export = self.widgets.frame(self.frame_master)
        self.frame_filter_and_export.pack(expand=True, fill=tk.BOTH, padx=2, pady=2)

        self.frame_filter:Frame = self.widgets.frame(self.frame_filter_and_export)
        self.frame_filter.pack(expand=True, fill=tk.BOTH, side=tk.LEFT)
        # Label informativo para os tipos de filtro
        self.label_info_filter = self.widgets.get_label(self.frame_filter)
        self.label_info_filter.config(text='Opções de filtro')
        self.label_info_filter.pack(expand=True, padx=2, pady=2)
        # Frame para agrupar a coluna selecionada pela usuário para filtrar os dados
        self.frame_column:Frame = self.widgets.frame(self.frame_filter)
        self.frame_column.pack(expand=True)
        # Frame para agrupar a caixa de texto.
        self.frame_text_find:Frame = self.widgets.frame(self.frame_filter)
        self.frame_text_find.pack()
        # Combobox para o usuário informar a coluna da planilha
        self.label_column:Label = self.widgets.get_label(self.frame_column)
        self.label_column.pack(side=tk.LEFT)
        self.label_column.config(text='Coluna (Filtro)')
        
        self.combobox_columns = self.widgets.get_combobox(
                                                self.frame_column,
                                                self.listColumnNames,
                                            )
        self.combobox_columns.set('Selecione uma coluna')
        self.combobox_columns.pack(side=tk.LEFT)
        
        # Caixa de texto para o usuário informar o texto a ser filtrado.
        self.label_find_text:Label = self.widgets.get_label(self.frame_text_find)
        self.label_find_text.pack(side=tk.LEFT)
        self.label_find_text.config(text='Texto (Filtro)')
        self.box_find_text:Entry = self.widgets.get_input_box(self.frame_text_find)
        self.box_find_text.pack(side=tk.LEFT)
        #------------------------------------------------#
        # Frame Radio PLANILHA OU TEXTO
        #------------------------------------------------#
        self.frame_radios_find: Frame = self.widgets.frame(self.frame_filter)
        self.frame_radios_find.pack(expand=True)
        # Radios para selecionar o tipo de filtro
        self.radio_text_or_sheet = tk.StringVar(value='from_text')
        self.radio_opt_from_text = self.widgets.get_radio_button(self.frame_radios_find)
        self.radio_opt_from_text.config(
                        text='Filtrar com texto', 
                        variable=self.radio_text_or_sheet,
                        value='from_text',
                    )
        self.radio_opt_from_text.pack(side=tk.LEFT)
        self.radio_opt_from_sheet = self.widgets.get_radio_button(self.frame_radios_find)
        self.radio_opt_from_sheet.config(
                            text='Filtrar com planilha',
                            variable=self.radio_text_or_sheet,
                            value='from_sheet',
                        )
        self.radio_opt_from_sheet.pack(side=tk.LEFT)

        #------------------------------------------------#
        # Frame para o botão de selecionar planilha filtro
        #------------------------------------------------#
        self.frame_select_sheet:Frame = self.widgets.frame(self.frame_filter)
        self.frame_select_sheet.pack(expand=True)
        # Botão para o usuário filtrar textos apartir de uma planilha.
        self.btn_select_sheet:Button = self.widgets.get_button(self.frame_select_sheet)
        self.btn_select_sheet.config(text='Planilha filtro', command=self.select_sheet_filter)
        self.btn_select_sheet.pack(side=tk.LEFT, expand=True)
        # Label para mostrar a planilha selecionada
        self.label_selected_sheet_filter: Label = self.widgets.get_label(self.frame_select_sheet)
        self.label_selected_sheet_filter.config(text='Arquivo filtro não selecionado')
        self.label_selected_sheet_filter.pack(side=tk.LEFT, expand=True)
        #---------------------------------------------------------------#
        # Frame para opções de exportar.
        #---------------------------------------------------------------#
        self.frame_options_export: Frame = self.widgets.frame(self.frame_filter_and_export)
        self.frame_options_export.pack(fill=tk.BOTH, expand=True, padx=1, pady=1) 
        # Label informativo
        self.label_export_info = self.widgets.get_label(self.frame_options_export)
        self.label_export_info.config(text='Opções de exportação')
        self.label_export_info.pack()
        # exportar para único arquivo
        self.radio_value_uniq_or_multi = tk.StringVar(value="uniq_file")
        self.radio_uniq_file = self.widgets.get_radio_button(self.frame_options_export)
        self.radio_uniq_file.config(
                                text='Arquivo', 
                                variable=self.radio_value_uniq_or_multi, 
                                value='uniq_file'
                            )
        self.radio_uniq_file.pack()
        # exportar para vários arquivos
        self.radio_multi_files = self.widgets.get_radio_button(self.frame_options_export)
        self.radio_multi_files.config(
                                text='Arquivos', 
                                variable=self.radio_value_uniq_or_multi, 
                                value='multi_files'
                            )
        self.radio_multi_files.pack()
        # exportar valores únicos de uma coluna.
        self.radio_uniq_column = self.widgets.get_radio_button(self.frame_options_export)
        self.radio_uniq_column.config(text='Coluna', variable=self.radio_value_uniq_or_multi, value='uniq_column')
        self.radio_uniq_column.pack()

        #---------------------------------------------------------------#
        # Frame OUTPUT
        #---------------------------------------------------------------#
        self.frame_output:Frame = self.widgets.frame(self.frame_master)
        self.frame_output.pack(fill=tk.BOTH, expand=True, padx=2, pady=2)
        # Label para exibir os textos de saída e mensagens para o usuário.
        self.label_output_texts: Label = self.widgets.get_label(self.frame_output)
        self.label_output_texts.config(text='-')
        self.label_output_texts.pack()
        # Label junto a barra de progresso.
        self.label_in_pbar:Label = self.widgets.get_label(self.frame_output)
        self.label_in_pbar.pack()
        # Barra de progresso infinita
        self.pbar:Progressbar = self.widgets.get_progress_bar(self.frame_output)
        self.pbar.pack(expand=True, fill=tk.BOTH)

        #---------------------------------------------------------------#
        # Botão para exportar a planilha filtrada.
        #---------------------------------------------------------------#
        self.btn_export: Button = self.widgets.get_button(self.frame_output)
        self.btn_export.config(command=self.action_check_and_export, text='Exportar Excel')
        self.btn_export.pack()

        # Botão voltar
        self.add_button_back(self.frame_master)

    def setDataFrame(self, d:DataFrame):
        d = d.astype('str')
        self._main_dataframe = d

    def getDataFrame(self) -> DataFrame:
        return self._main_dataframe    

    def check_values_column(self) -> bool:
        """
            Verificar se a coluna foi selecionada pelo usuário, exibir mensagem de
        erro se necessário.
        """
        if self.combobox_columns.get() is None:
            show_warnnings('Selecione uma coluna para prosseguir')
            return False
        if not self.format_data.existsColumn(self.combobox_columns.get(), iqual=True):
            show_warnnings('Selecione uma coluna para prosseguir')
            return False
        return True
    
    def select_sheet_filter(self):
        """
            Selecionar uma planilha para ser usada como filtro.
        """
        s = self.controller.openFiles.open_file_sheet()
        self.sheetFilter:FileSheet = FileSheet(s)
        self.label_selected_sheet_filter.config(text=self.sheetFilter.basename())

    def _check_sheet_filter(self) -> bool:
        """
            Checar o arquivo de filtro, exibir mensagens de erro se necessário.
        """
        if self.sheetFilter is None:
            show_warnnings('Selecione uma planilha de filtro para prosseguir')
            return False
        
        d = self._df_sheet_filter()
        if d.empty:
            show_warnnings(f'O arquivo está vazio: {self.sheetFilter.basename()}')
            return False
        if not FormatData(data=d).existsColumn(self.combobox_columns.get(), iqual=True):
            show_warnnings(
                f'A coluna:{self.combobox_columns.get()}\nnão existe no arquvo:\n{self.sheetFilter.basename()}'
            )
            return False
        return True
    
    def action_concat_columns(self):
        """
            Ação conectada ao botão concatenar
        """
        t = Thread(target=self._exec_concat)
        t.start()

    def _exec_concat(self):
        if not self.format_data.existsColumn(self.combo_concat_1.get()):
            show_warnnings(
            f'A coluna:\n{self.combo_concat_1.get()} não existe!'
            )
            return
        if not self.format_data.existsColumn(self.combo_concat_2.get()):
            show_warnnings(
            f'A coluna:\n{self.combo_concat_2.get()} não existe!'
            )
            return
        if not self.format_data.existsColumn(self.combo_concat_3.get()):
            show_warnnings(
            f'A coluna:\n{self.combo_concat_3.get()} não existe!'
            )
            return

        self.start_pbar()
        self.format_data.concatColumns(
            columns=[
                self.combo_concat_1.get(),
                self.combo_concat_2.get(),
                self.combo_concat_3.get(),
            ],
            separator='_'
        )
        self.setDataFrame(self.format_data.data)
        self.update_comboboxs()
        self.stop_pbar()
    
    def act_btn_del_col(self):
        """
            Ação conectada ao botão para remover colunas
        """
        t = Thread(target=self._remove_columns)
        t.start()

    def _remove_columns(self):
        if not self.format_data.existsColumn(self.combo_remove_colums.get()):
            show_warnnings(
            f'Selecione uma coluna válida, a coluna:\n{self.combo_remove_colums.get()} não existe!'
            )
            return

        self.start_pbar()
        # Deletando uma coluna
        self.setDataFrame(self.getDataFrame().drop(self.combo_remove_colums.get(), axis=1))
        self.update_comboboxs()
        self.stop_pbar()
    
    def act_btn_remove_null(self):
        if self.running == True:
            return
        
        self.running = True
        self.start_pbar()
        t = Thread(target=self._rm_null)
        t.start()
        
    def _rm_null(self):
        self.remove_null_lines()
        self.stop_pbar()
        self.running = False
    
    def remove_null_lines(self):
        """
            Remover linhas vazias da coluna selecionada se o usuário solicitar.
        """
        if self.format_data is None:
            return
        #c = self.combobox_columns.get()
        c = self.combobox_yesno.get()
        if not self.format_data.existsColumn(c):
            self.set_text_label_output('Selecione uma coluna para prosseguir')
            return
        self.set_text_label_output(f'Removendo linhas vazias da coluna:\n{c}')
        self.setDataFrame(self.getDataFrame()[self.getDataFrame()[c] != ''])
        df = self.getDataFrame().dropna(subset=[c])
        df = df[df[c] != 'nan']
        self.setDataFrame(df)
        self.format_data = None
        self.format_data = FormatData(data=self.getDataFrame())
        self.set_text_label_output('OK')
    
    def action_check_and_export(self):
        """
            Ação conectada ao botão de exportar.
        Verifica alguns valores antes de prosseguir com a exportação dos dados.
        """
        if self.running == True:
            return
        if not self.check_values_column():
            return
    
        # Verificar o tipo de filtro, e confirmar se está ok.
        if self.radio_value_uniq_or_multi.get() == 'uniq_column':
           pass
        else:
            if self.radio_text_or_sheet.get() == 'from_text':
                # o usuário selecionou filtro com texto.
                if (self.box_find_text.get() is None) or (self.box_find_text.get() == ''):
                    show_warnnings('Digite um texto para prosseguir.')
                    return
            elif self.radio_text_or_sheet.get() == 'from_sheet':
                # O usuário selecionou filtrar com arquivo.
                if not self._check_sheet_filter():
                    return
        
        self.running = True
        th = Thread(target=self._export)
        th.start()

    def _export(self):
        """
            Verifica as opções de exportar (um ou mais arquivos), em seguida
        gera um Thread apropriada para as opções selecionadas e executa a Thread.
        """
        if self.radio_value_uniq_or_multi.get() == 'uniq_file':
            th = Thread(target=self._to_uniq_file)
        elif self.radio_value_uniq_or_multi.get() == 'multi_files':
            th = Thread(target=self._to_multi_files)
        elif self.radio_value_uniq_or_multi.get() == 'uniq_column':
            th = Thread(target=self._to_uniq_column)
        else:
            return

        self.running = True
        self.start_pbar()
        th.start()
        while True:
            time.sleep(2)
            if self.running == False:
                break
        self.stop_pbar()
        
    def _to_multi_files(self):
        
        columnName = self.combobox_columns.get()
        # Verificar/salvar o tipo de texto a ser filtrado pelo usuário.
        if self.radio_text_or_sheet.get() == 'from_text':
            values:List[str] = [self.box_find_text.get()]
        elif self.radio_text_or_sheet.get() == 'from_sheet':
            values:List[str] = FormatData(data=self._df_sheet_filter()).getColumnUniq(columnName)
            
        outdir:Directory = self.controller.saveDir.concat_dir('export')
        outdir.mkdir()
        maxNum:int = len(values)
        numErros:int = 0
        for num, value in enumerate(values):
            self.set_text_label_output(f'Filtrando: Coluna {columnName} | Texto: {value}')
            self.set_text_in_pbar(f'Progresso: {num+1} de {maxNum} | Erros: {numErros} |')
            #d:DataFrame = self.format_data.getElements(column=columnName, iqual=True, value=value)
            d:DataFrame = self._main_dataframe[self._main_dataframe[columnName] == value]
            if not d.empty:
                save_dataframe(data=d, outFile=outdir.joinFile(f'{columnName}-{value}.xlsx'))
            else:
                numErros += 1
                print(f'Não encontrado: Coluna {columnName} | Texto: {value}')
        self.running = False
        self.set_text_label_output(f'Arquivos exportados em: {outdir.basename()}')
        #self.set_text_in_pbar('OK')

    def _to_uniq_file(self):
        """
            Filtrar o texto informado pelo usuário e exportar para um arquivo
        """
        
        columnName = self.combobox_columns.get()
        # Verificar/salvar o tipo de texto a ser filtrado pelo usuário.
        if self.radio_text_or_sheet.get() == 'from_text':
            values:List[str] = [self.box_find_text.get()]
        elif self.radio_text_or_sheet.get() == 'from_sheet':
            values:List[str] = FormatData(data=self._df_sheet_filter()).getColumnUniq(columnName)
            
        # Guardar os filtros aplicados em uma lista
        df_elements:List[DataFrame] = []
        maxNum:int = len(values)
        for num, value in enumerate(values):
            self.set_text_label_output(f'Filtrando {num+1} de {maxNum}: | Coluna {columnName} | Texto {value} |')
            #d:DataFrame = self.format_data.getElements(column=columnName, iqual=True, value=value)
            d:DataFrame = self._main_dataframe[self._main_dataframe[columnName] == value]
            if not d.empty:
                df_elements.append(d)
            else:
                print(f'Não encontrado: Coluna {columnName} | Texto: {value}')

        if df_elements == []:
            self.set_text_label_output(f'Nenhum valor encontrado na coluna: {columnName}')
            self.running = False
            return

        final_df = pandas.concat(df_elements)
        outfile: File = self.controller.saveDir.joinFile(f'{columnName}.xlsx')
        self.set_text_label_output(f'Exportando: {outfile.basename()}')
        save_dataframe(data=final_df, outFile=outfile)
        self.running = False

    def _to_uniq_column(self):
        # Obter apenas valores não duplicados, da coluna que o usuário selecionou para exportar.
        
        values:List[object] = self.format_data.getColumnUniq(self.combobox_columns.get())
        if len(values) < 1:
            self.running = False
            return
        _d = {self.combobox_columns.get(): values,}
        df = DataFrame(_d)
        outfile = self.controller.saveDir.joinFile(f'filtro-{self.combobox_columns.get()}.xlsx')
        save_dataframe(data=df, outFile=outfile)
        self.running = False
        self.set_text_label_output(f'Arquivo exportado: {outfile.basename()} OK')

    def _df_sheet_filter(self) -> DataFrame:
        """
            Ler a planilha que o usuário selecionou para ser o filtro
        """
        input_data = InputDataSheet(fileSheet=self.sheetFilter)
        try:
            d:DataFrame = input_data.dataframe()
        except Exception as e:
            print(e)
            return DataFrame()
        else:
            return d.astype('str')

    def set_text_label_output(self, text:str) -> None:
        if text is not None:
            self.label_output_texts.config(text=text)

    def set_text_in_pbar(self, text):
        if text is None:
            return
        self.label_in_pbar.config(text=text)

    def start_pbar(self):
        self.pbar.start(10)

    def stop_pbar(self):
        self.pbar.stop()

    def operation_load_values(self):
        t = Thread(target=self.load_input_sheets)
        t.start()
        
    def _init_data(self) -> None:
        """
            Thread para iniciar a leitura dos dados quando essa janela é carregada.
        """
        
        self.start_pbar()
        t = Thread(self.load_input_sheets)
        t.start()
        while True:
            time.sleep(1)
            if self.running == False:
                break
        self.stop_pbar()
        self.main_threading = None

    def load_input_sheets(self) -> bool:
        """
            Este método deve ser chamado logo após a janela ser carregada, 
        preferencialmente em uma Thread, pois apartir desse método serão lidas a(s) 
        planilhas que o usuário selecionou na janela anterior, o DataFrame resultante 
        também será carregado na memoria RAM.
        """
        self.running = True
        # Inserir os DataFrames de cada planilha nessa lista.
        list_input_elements:List[DataFrame] = []
        self.list_stop_events.clear()
        self.start_pbar()
        for num, f in enumerate(self.selectedSheets):
            #load:LoadExcel = LoadExcel(sheet=f)
            input_data = InputDataSheet(fileSheet=f)
            self.list_stop_events.append(input_data.stop_event)
            current_thread = threading.Thread(target=input_data.dataframe)
            current_thread.start()
            #
            self.set_text_label_output(
                f'Lendo: {num+1} de {len(self.selectedSheets)} | {f.basename()} | {f.human_size()}'
            )
            #
            while True:
                if input_data.running_df == False:
                    break
                time.sleep(1)
                self.set_text_in_pbar(f'Progresso: {input_data.currentProgress:.2f}%')

            df:DataFrame = input_data.dataframe()
            if not df.empty:
                list_input_elements.append(df)
                
        if len(list_input_elements) < 1:
            print(f'{__class__.__name__} Lista de DataFrame vazios')
            self.set_text_label_output(f'Erro: planilhas vazias')
            self.set_text_in_pbar('-')
            show_warnnings('As planilhas estão vazias')
            self.running = False
            self.stop_pbar()
            return False

        self.setDataFrame(pandas.concat(list_input_elements))
        self.update_comboboxs()
        self.running = False
        self.stop_pbar()
        self.set_text_label_output('Leitura finalizada.')
        self.set_text_in_pbar('-')
        return True

    def update_comboboxs(self) -> None:
        """
            Atualizar os valores dos combobox que usam o cabeçalho do DataFrame
        """
        self.combobox_columns['values'] = self.getDataFrame().columns.tolist()
        self.combobox_yesno['values'] = self.getDataFrame().columns.tolist()
        self.combo_remove_colums['values'] = self.getDataFrame().columns.tolist()
        self.combo_concat_1['values'] = self.getDataFrame().columns.tolist()
        self.combo_concat_2['values'] = self.getDataFrame().columns.tolist()
        self.combo_concat_3['values'] = self.getDataFrame().columns.tolist()
        self.format_data = FormatData(data=self.getDataFrame())
        
    def set_size_screen(self):
        self.parent.title("Home => Selecionar ação => Filtrar Texto em planilha")
        self.parent.geometry("580x430")

    def check_selected_sheets(self) -> bool:
        """
            Verifica se o usuário selecionou pelo menos uma planilha.
        Se a lista com arquivos Excel/CSV que é gerada apartir de uma pasta
        selecionada na janela anterior estiver vazia, será exibida uma mensagem de
        erro e o programa será direcionado para HomePage para que o usuário possa selecionar
        arquivos .csv/.xlsx
        """
        if len(self.controller.inputFiles) < 1:
            return False
        self.selectedSheets.clear()
        for f in self.controller.inputFiles:
            if (f.is_excel()) or (f.is_csv()):
                self.selectedSheets.append(f)
        if len(self.selectedSheets) < 1:
            return False
        self.numFiles = len(self.selectedSheets)
        return True
    
    def update_state(self):
        if not self.check_selected_sheets():
            show_warnnings('Selecione pelo menos uma planilha para prosseguir')
            self.controller.to_page('/home')
            return
        self.operation_load_values()


# Arquivos para excel
class PageFilesToExcel(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.page_name = '/home/select_actions/folder_to_excel'
        self.frame_master = self.widgets.frame(self)
        self.frame_master.pack()
        self.initUI()

    def initUI(self):
        
        # Frame
        self.frame1 = self.widgets.frame(self.frame_master)
        self.frame1.pack()
        # Frame
        self.frame2 = self.widgets.frame(self.frame_master)
        self.frame2.pack()
        # Frame
        self.frame3 = self.widgets.frame(self.frame_master)
        self.frame3.pack()
        # Label
        self.label_info = self.widgets.get_label(self.frame1)
        self.label_info.config(text='-')
        self.label_info.pack()
        # Label pbar
        self.label_pbar = self.widgets.get_label(self.frame2)
        self.label_pbar.config(text='-')
        self.label_pbar.pack()
        # Label progress
        self.label_progress = self.widgets.get_label(self.frame2)
        self.label_progress.config(text='0%')
        self.label_progress.pack()
        # Pbar
        self.pbar = self.widgets.get_progress_bar(self.frame2)
        self.pbar.pack()

        # button
        self.btn_action = self.widgets.get_button(self.frame3)
        self.btn_action.config(text='Executar', command=self.run_action)
        self.btn_action.pack()
        # botão voltar
        self.btn_back = self.widgets.get_button(self.frame_master)
        self.btn_back.config(
                text='Voltar', 
                command=lambda: self.controller.navigatorPages.pop(),
            )
        self.btn_back.pack()
    
    def set_size_screen(self):
        self.parent.title("OCR Tool => Selecionar ação => Arquivos para Excel")
        self.parent.geometry('500x250')
        
    def run_action(self):
        th = Thread(target=self._files_to_sheet)
        th.start()

    def _files_to_sheet(self):
        self.running = True
        self.start_pbar()
        e = Thread(target=self._execute)
        e.start()
        self.label_info.config(text='Aguarde...')
        while True:
            if self.running == True:
                break
            time.sleep(1)
        self.stop_pbar()
        self.label_info.config(text='OK')

    def _execute(self):
        input_files:List[File] = InputFiles(
                                        inputDir=self.controller.inputDir, maxFiles=2000
                                    ).getAllFiles()
        
        out_file = self.controller.saveDir.joinFile(f'arquivos-para-excel.xlsx')
        listDirName = []
        listFileName = []
        listFiles = []
        listExtensions = []
        for f in input_files:
            listDirName.append(f.dirname())
            listFileName.append(f.basename())
            listFiles.append(f.absolute())
            listExtensions.append(f.extension())

        t = {
            'ARQUIVOS': listFiles,
            'PASTA': listDirName,
            'NOME': listFileName,
            'TIPO': listExtensions,
        }
        df = DataFrame(t)
        save_dataframe(data=df, outFile=out_file)
        self.running = False

    def start_pbar(self):
        self.running = True
        self.pbar.start(10)

    def stop_pbar(self):
        self.running = False
        self.pbar.stop()

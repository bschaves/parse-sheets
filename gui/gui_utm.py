#!/usr/bin/env python3
#

import os
import time
import pandas
import numpy
from typing import (List)
import threading
from threading import Thread, Event

from doc_convert.utils._version import (
        __version__, 
        __author__, 
        __update__,
        __version_lib__,
        __url__,
)

from doc_convert import (
    File, 
    FileSheet,
    Directory, 
    InputFiles,
    InputDataSheet,
    DataFrame,
    FormatData,
    KERNEL_TYPE,
)

from doc_convert.soup_data.utm_convert import (
    RouteLatLon,
    UtmPoint,
    LatLongPoint,
    ConvertPoints,
    CreateMap,
)

import tkinter as tk
from tkinter.ttk import (
    Entry, 
)

from gui.gui_utils import (
    show_warnnings,
    AppPage,
)

# Gerar mapas em HTML
class PageCreateMaps(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.page_name = '/home/select_actions/maps'
        self.frame_master = self.widgets.frame(self)
        self.frame_master.pack()
        #
        self.selected_sheets:List[FileSheet] = []
        self._num_files:int = 0
        self._current_progress:float = 0
        #
        #self.data:List[DataFrame] = []
        self.main_df:DataFrame = None
        self.list_erros:List[str] = []
        self.num_erros:int = 0
        #
        self.list_sucess:List[str] = []
        self.num_sucess:int = 0
        #
        self.running = False
        self.listColumnNames: List[str] = ['-']
        self.initUI()

    def initUI(self):
        #------------------------------------------------------#
        # Frame para combobox 
        #------------------------------------------------------#
        self.frame_select_columns = self.widgets.frame(self.frame_master)
        self.frame_select_columns.pack()
        # Label informativo
        self.label_top_info = self.widgets.get_label(self.frame_select_columns)
        self.label_top_info.config(text='Selecione as colunas de Latitude e Longitude')
        self.label_top_info.pack(expand=True, fill='both', padx=1, pady=1)
        #
        self.frame_combo_lat = self.widgets.frame(self.frame_select_columns)
        self.frame_combo_lat.pack(expand=True, fill='both', padx=1, pady=1)
        #
        self.frame_combo_longitude = self.widgets.frame(self.frame_select_columns)
        self.frame_combo_longitude.pack(expand=True, fill='both', padx=1, pady=1)
        #
        #

        # Frame
        self.frame_top_info = self.widgets.frame(self.frame_master)
        self.frame_top_info.pack()
        # Frame
        self.frame_pbar = self.widgets.frame(self.frame_master)
        self.frame_pbar.pack()
        # Frame
        self.frame_buttons = self.widgets.frame(self.frame_master)
        self.frame_buttons.pack()

        # Label
        self.label_info_out_file = self.widgets.get_label(self.frame_top_info)
        self.label_info_out_file.config(text='-')
        self.label_info_out_file.pack()

        #------------------------------------------------------#
        # Combobox para Selecionar coluna latitude
        #------------------------------------------------------#
        # Label Latitude
        self.label_latitude = self.widgets.get_label(self.frame_combo_lat)
        self.label_latitude.config(text='Coluna Latitude: ')
        self.label_latitude.pack(side=tk.LEFT, expand=True, padx=1, pady=1)
        # Combobox ao lado do label Latitude.
        self.combobox_lat = self.widgets.get_combobox(self.frame_combo_lat, self.listColumnNames,)
        self.combobox_lat.set('-')
        self.combobox_lat.pack(side=tk.LEFT, expand=True, padx=1, pady=1, fill='both')

        # Label Longitude
        self.label_longitude = self.widgets.get_label(self.frame_combo_longitude)
        self.label_longitude.config(text='Coluna Longitude: ')
        self.label_longitude.pack(side=tk.LEFT, expand=True, padx=1, pady=1)

        # Combobox ao lado do label Latitude.
        self.combobox_long = self.widgets.get_combobox(
            self.frame_combo_longitude, 
            self.listColumnNames,
        )
        self.combobox_long.set('-')
        self.combobox_long.pack(side=tk.LEFT, expand=True, padx=1, pady=1, fill='both')

        # Label Sucessos e Erros
        self.label_sucess_erros = self.widgets.get_label(self.frame_pbar)
        self.label_sucess_erros.config(text=f'Sucessos: {self.num_sucess} | Erros: {self.num_erros}')
        self.label_sucess_erros.pack(padx=2, pady=2, fill='both')

        # Label pbar
        self.label_pbar = self.widgets.get_label(self.frame_pbar)
        self.label_pbar.config(text='-')
        self.label_pbar.pack(padx=3, pady=2)

        # Label progress
        self.label_progress = self.widgets.get_label(self.frame_pbar)
        self.label_progress.config(text='0%')
        self.label_progress.pack()

        # Pbar
        self.pbar = self.widgets.get_progress_bar(self.frame_pbar)
        self.pbar.pack()

        # botão voltar
        self.btn_back = self.widgets.get_button(self.frame_buttons)
        self.btn_back.config(
                text='Voltar', 
                command=lambda: self.controller.navigatorPages.pop(),
            )
        self.btn_back.pack(side=tk.LEFT, expand=True, padx=1, pady=1)

        # botão Criar Mapa
        self.btn_action = self.widgets.get_button(self.frame_buttons)
        self.btn_action.config(text='Criar Mapa', command=self.run_action)
        self.btn_action.pack(side=tk.LEFT, expand=True, padx=1, pady=1)

        # botão para abrir a pasta de destino.
        self.btn_open_folder = self.widgets.get_button(self.frame_buttons)
        self.btn_open_folder.config(text='Abrir Pasta/Destino', command=self._open_destination_folder)

    def update_labels_sucess_erros(self):
        self.label_sucess_erros.config(text=f'Sucessos: {self.num_sucess} | Erros: {self.num_erros}')

    def set_size_screen(self):
        self.parent.title("Gerar Mapa")
        self.parent.geometry('500x250')

    def update_state(self):
        if len(self.controller.inputFiles) < 1:
            show_warnnings('Selecione arquivos para prosseguir!')
            self.controller.navigatorPages.pop()
            return
        self.selected_sheets.clear()
        self.selected_sheets = [FileSheet(f.absolute()) for f in self.controller.inputFiles if f.is_excel()]
        self._num_files = len(self.selected_sheets)

        if self._num_files < 1:
            show_warnnings('Selecione arquivo(s) excel para prosseguir')
            self.controller.navigatorPages.pop()
            return
        
        print(f'\n\nAtualizando página: {self.page_name}')
        self.label_info_out_file.config(text=f'Salvar em: {self.controller.saveDir.basename()}')
        #
        thread_update = threading.Thread(target=self._load_values)
        thread_update.start()
        
    def _update_combos(self):
        """
            Atualizar as caixas para seleção de coordenadas.
        """
        self.combobox_lat['values'] = self.main_df.columns.tolist()
        self.combobox_long['values'] = self.main_df.columns.tolist()

    def _load_values(self) -> None:
        """
            Atualizar os valores dos arquivos selecionados.
        """
        self.main_df = None
        #
        self.start_pbar()
        _current_file:FileSheet = self.selected_sheets[0]
        self.label_pbar.config(text=f'Lendo: {_current_file.basename()}')
        #self.main_df = InputDataSheet(fileSheet=_current_file).dataframe()
        self.main_df = pandas.read_excel(_current_file.absolute())
        #
        self._update_combos()
        self.label_pbar.config(text='-')
        self.stop_pbar()

    def run_action(self):
        if self.running == True:
            show_warnnings('Já existe uma operação em andamento. Aguarde...')
            return
        th = Thread(target=self._sheet_to_html_maps)
        th.start()

    def _sheet_to_html_maps(self):
        self.start_pbar()
        e = Thread(target=self._execute)
        e.start()
        self.label_pbar.config(text='Aguarde...')
        while True:
            if self.running == False:
                break
            time.sleep(1)
        self.stop_pbar()
        self.label_pbar.config(text='OK')
        self.label_progress.config(text='100%')

    def _execute(self):
        if not self.combobox_lat.get() in self.main_df.columns.tolist():
            show_warnnings('Selecione uma coluna de Latitude')
            self.stop_pbar()
            return
        if not self.combobox_long.get() in self.main_df.columns.tolist():
            show_warnnings('Selecione uma coluna de Longitude')
            self.stop_pbar()
            return
        #
        self.label_pbar.config(text=f'Criando o mapa(s) em: {self.controller.saveDir.basename()}')
        
        # Iterar sobre cada planilha, em seguida gerar o mapa correspondente.
        for num, f in enumerate(self.selected_sheets):
            # Atualizar labels na tela
            self._current_progress = (num/self._num_files) * 100
            self.label_progress.config(text=f'{self._current_progress:.2f}%')
            self.label_pbar.config(text=f'Criando mapa do arquivo: {f.basename()} | {num+1} de {self._num_files}')
            
            # Checar os dados da planilha.
            current_df_lat_lon = InputDataSheet(fileSheet=f).dataframe()
            # Substituir strings "nan" por valores NaN
            current_df_lat_lon = current_df_lat_lon.replace("nan", numpy.nan)
            #  Remover linhas que contêm NaN em qualquer coluna
            current_df_lat_lon = current_df_lat_lon.dropna(
                subset=[self.combobox_lat.get(), self.combobox_long.get()]
            )
            
            if not self.combobox_lat.get() in current_df_lat_lon:
                self.list_erros.append(f.absolute())
                self.num_erros = len(self.list_erros)
                self.update_labels_sucess_erros()
                continue
            if not self.combobox_long.get() in current_df_lat_lon:
                self.list_erros.append(f.absolute())
                self.num_erros = len(self.list_erros)
                self.update_labels_sucess_erros()
                continue

            # Gerar um DataFrame de duas colunas latitude/longitude
            current_df_lat_lon = current_df_lat_lon[[self.combobox_lat.get(), self.combobox_long.get()]]
            
            try:
                # Gerar os objetos de Rota apartir das coordenadas do DataFrame()
                values = list(current_df_lat_lon.itertuples(index=False, name=None))
                points:List[LatLongPoint] = [LatLongPoint(lat, lon) for lat, lon in values]
                route:RouteLatLon = RouteLatLon(points)
                CreateMap(route).to_html(
                    self.controller.saveDir.joinFile(f'{f.basename()}.html').absolute()
                )
            except Exception as e:
                print(f'\n{__class__.__name__} - {e}\n')
                self.list_erros.append(f.absolute())
                self.num_erros = len(self.list_erros)
                self.update_labels_sucess_erros()
            else:
                self.list_sucess.append(f.absolute())
                self.num_sucess = len(self.list_sucess)
                self.update_labels_sucess_erros()
        #
        self.btn_open_folder.pack(side=tk.LEFT, expand=True, padx=1, pady=1)
        self.running = False

    def _open_destination_folder(self):
        """
            Abrir a pasta de destino com os mapas HTML.
        """
        if KERNEL_TYPE == 'Linux':
            os.system(f'xdg-open {self.controller.saveDir.absolute()}')
        elif KERNEL_TYPE == 'Windows':
            # chrome_path = r"C:\Program Files\Google\Chrome\Application\chrome.exe"
            # subprocess.run([chrome_path, outfile])
            os.startfile(self.controller.saveDir.absolute())

    def start_pbar(self):
        self.running = True
        self.pbar.start(10)

    def stop_pbar(self):
        self.running = False
        self.pbar.stop()


# Gerar converter coordenadas
class PageConvertToLatLon(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.page_name = '/home/select_actions/to_latlon'
        self.frame_master = self.widgets.frame(self)
        self.frame_master.pack()
        #
        self.selected_sheets:List[FileSheet] = []
        self._num_files:int = 0
        self._current_progress:float = 0
        self.initUI()

    def initUI(self):
        #================================================#
        # CONVETER DE PONTO
        #================================================#
        self.frame_point = self.widgets.frame(self.frame_master)
        self.frame_point.pack()
        #
        #
        self.label_conv_point = self.widgets.get_label(self.frame_point)
        self.label_conv_point.config(text='Converter Ponto UTM para Latitude e Longitude')
        self.label_conv_point.pack()
        #
        # Container para entrada de texto UTM Leste
        self.frame_utm_east = self.widgets.frame(self.frame_point)
        self.frame_utm_east.pack(expand=True, fill='both', padx=2, pady=1)
        
        self.label_text_utm_east = self.widgets.get_label(self.frame_utm_east)
        self.label_text_utm_east.config(text='UTM Leste:')
        self.label_text_utm_east.pack(side=tk.LEFT, expand=True, fill='both', padx=2, pady=1)

        self.input_text_easting = Entry(self.frame_utm_east)
        self.input_text_easting.pack(expand=True, fill='both', padx=2, pady=1)
        #
        # Container para entrada de texto UTM Norte
        self.label_text_utm_nort = self.widgets.get_label(self.frame_point)
        self.label_text_utm_nort.config(text='UTM Norte:')
        self.label_text_utm_nort.pack(side=tk.LEFT, expand=True, fill='both', padx=2, pady=1)

        self.input_text_northing = Entry(self.frame_point)
        self.input_text_northing.pack(expand=True, fill='both', padx=2, pady=1)
        # Frame Result
        self.frame_result = self.widgets.frame(self.frame_master)
        self.frame_result.pack()
        # Label do resultado da conversão
        self.label_result = self.widgets.get_label(self.frame_result)
        # Botão copiar
        self.btn_copy = self.widgets.get_button(self.frame_result)
        self.btn_copy.config(
            text='Copiar', 
            command=self.copy_lat_lon,
        )
        #================================================#
        self.frame_zone = self.widgets.frame(self.frame_master)
        self.frame_zone.pack()
        self.label_zone_text = self.widgets.get_label(self.frame_zone)
        self.label_zone_text.config(text='Informações da Zona')
        self.label_zone_text.pack()
        #
        #================================================#
        # COMBO ZONE NUM
        #================================================#
        self.frame_zone_num = self.widgets.frame(self.frame_zone)
        self.frame_zone_num.pack(expand=True, fill='both', padx=2, pady=1)
        #
        self.label_zone_num = self.widgets.get_label(self.frame_zone_num)
        self.label_zone_num.config(text='Número da Zona:')
        self.label_zone_num.pack(side=tk.LEFT)
        #
        self.combo_zone_num = self.widgets.get_combobox(
            parent=self.frame_zone_num,
            values=[x for x in range(1, 60)],
        )
        self.combo_zone_num.pack(side=tk.LEFT)
        self.combo_zone_num.set('20')
        
        #================================================#
        # COMBO ZONE LETTER
        #================================================#
        self.valeus_letter = [
            'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        ]
        self.frame_zone_letter = self.widgets.frame(self.frame_zone)
        self.frame_zone_letter.pack()
        #
        self.label_letter_zone = self.widgets.get_label(self.frame_zone_letter)
        self.label_letter_zone.config(text='Letra da Zona:')
        self.label_letter_zone.pack(side=tk.LEFT)
        #
        self.combo_zone_letter = self.widgets.get_combobox(
            parent=self.frame_zone_letter,
            values=self.valeus_letter,
        )
        self.combo_zone_letter.pack(side=tk.LEFT)
        self.combo_zone_letter.set('K')

        #================================================#
        # container convert
        #================================================#
        self.frame_pbar = self.widgets.frame(self.frame_master)
        self.frame_pbar.pack(expand=True, fill='both', padx=2, pady=2)

        # barra de progresso
        self.pbar = self.widgets.get_progress_bar(self.frame_pbar)
        self.pbar.pack()

        # Frame Radios
        self.frame_radios = self.widgets.frame(self.frame_master)
        self.frame_radios.pack()    

        # Radio para selecionar o tipo de conversão
        self.radio_value_convert:tk.StringVar = tk.StringVar(value='point')

        # De ponto
        self.radio_from_point = self.widgets.get_radio_button(self.frame_radios)
        self.radio_from_point.config(
                        text='De Ponto', 
                        variable=self.radio_value_convert,
                        value='point',
                    )
        self.radio_from_point.pack(side=tk.LEFT)

        # De arquivo
        self.radio_from_file = self.widgets.get_radio_button(self.frame_radios)
        self.radio_from_file.config(
                        text='De Arquivo', 
                        variable=self.radio_value_convert,
                        value='file',
                    )
        self.radio_from_file.pack(side=tk.LEFT)

        # Frame Botões
        self.frame_buttons = self.widgets.frame(self.frame_master)
        self.frame_buttons.pack()

        # Botão voltar
        self.add_button_back(self.frame_buttons)
        self.btn_back.pack(side=tk.LEFT)

        # Botão Converter
        self.btn_convert = self.widgets.get_button(self.frame_buttons)
        self.btn_convert.config(text='Converter', command=self.run_convert)
        self.btn_convert.pack(side=tk.LEFT)

    def copy_lat_lon(self):
        self.parent.clipboard_clear()
        _text = self.label_result['text']
        self.parent.clipboard_append(_text)
        self.parent.update()

    def run_convert(self):
        e = threading.Thread(target=self._execute)
        e.start()

    def _execute(self):
        
        if self.radio_value_convert.get() == 'point':
            try:
                point_utm = UtmPoint(
                    east=self.input_text_easting.get(),
                    north=self.input_text_northing.get(),
                    zone=self.combo_zone_num.get(),
                    letter=self.combo_zone_letter.get(),
                )
            except Exception as e:
                print(e)
                show_warnnings(e)
            else:
                latlon:LatLongPoint = point_utm.to_latlong()
                if latlon is None:
                    show_warnnings('Falha na conversão, tente novamente!')
                    return
                self.label_result.config(
                    text=f'{latlon.lat}, {latlon.long}'
                )
                self.label_result.pack(padx=3, pady=2, side=tk.LEFT)
                self.btn_copy.pack(padx=2, pady=2)
            return

    def set_size_screen(self):
        self.parent.title('Converter UTM para Latitude e Longitude')
        self.parent.geometry('550x300')

    def update_state(self):
        return super().update_state()

    def run_action(self):
        pass
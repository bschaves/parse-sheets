#!/usr/bin/env python3
#
from __future__ import annotations
import os, shutil
from typing import (List, Callable, Optional, get_args, Dict)
from threading import Thread

from doc_convert.utils._version import (
    __version__, 
    __author__, 
    __update__,
    __version_lib__,
    __url__,
)

from doc_convert import (
    File, 
    FileSheet,
    FileJson,
    Directory, 
    InputFiles,
    OcrAppDirs,
    KERNEL_TYPE,
    UserPrefs,
)

import tkinter as tk
from tkinter import (
        Tk,
        Menu,
        ttk,  
        filedialog, 
        messagebox,
)

from tkinter.ttk import (
        Frame,
)

from gui.gui_utils import (
    show_warnnings,
    SharedValues,
    GetWidgets,
    AppPage,
    ControllerApp,
    Navigator,
)

from gui.gui_sheets import (
    PageFilesToExcel,
    PageSoupSheets,
)

from gui.gui_utm import (
    PageCreateMaps,
    PageConvertToLatLon,
)

#========================================================#
# Página Mover arquivos.
#========================================================#
class PageMoveFiles(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.page_name = '/home/select_actions/move_files'
        self._num_files = 0
        self.selected_files:List[File] = []
        self.initUI()

    def initUI(self):
        self.frame_top_info = self.widgets.frame(self.frame_master)
        self.frame_top_info.pack()
        #
        self.label_top_info = self.widgets.get_label(self.frame_top_info)
        self.label_top_info.config(text=f'Arquivos para Mover: {self._num_files}')
        self.label_top_info.pack(padx=2, pady=2)
        #
        self.label_info_out_dir = self.widgets.get_label(self.frame_top_info)
        self.label_info_out_dir.config(text=f'Pasta de destino: {self.controller.saveDir.basename()}')
        self.label_info_out_dir.pack(padx=2, pady=2)

        # Frame Máximo de arquivos
        self.frame_max_files = self.widgets.frame(self.frame_master)
        self.frame_max_files.pack(expand=True, fill='both', padx=2, pady=2)

        # 
        self.label_select_max_files = self.widgets.get_label(self.frame_max_files)
        self.label_select_max_files.config(text='Máximo de arquivos')
        self.label_select_max_files.pack(padx=3, pady=1, fill='both', expand=True, side=tk.LEFT)
    
        # combo para selecionar o máximo de arquivos.
        self.combo_max_files = self.widgets.get_combobox(
            self.frame_max_files,
            [5, 10, 20, 50, 100, 200, 500, 1000],
        )
        self.combo_max_files.pack(expand=True, fill='both', padx=1, pady=1)
        self.combo_max_files.set(50)

        # Label Progresso
        self.label_progress = self.widgets.get_label(self.frame_top_info)
        self.label_progress.config(text='Progresso: 0%')
        self.label_progress.pack(padx=2, pady=2)

        # Frame Botões
        self.frame_buttons = self.widgets.frame(self.frame_master)
        self.frame_buttons.pack()
    
        # Botão voltar
        self.add_button_back(self.frame_buttons)
        self.btn_back.pack(side=tk.LEFT)

        # Botão Mover
        self.btn_move = self.widgets.get_button(self.frame_buttons)
        self.btn_move.config(text='Mover', command=self.move_files)
        self.btn_move.pack()

    def move_files(self):
        e = Thread(target=self._run_move)
        e.start()

    def _run_move(self):
        self.start_pbar()
        count = 0
        count_dir = 1
        num_dirs = 0
        max_files = int(self.combo_max_files.get())
        out_dir = self.controller.saveDir.concat_dir(f'{max_files}-{num_dirs}')
        out_dir.mkdir()
        for file in self.selected_files:
            count += 1
            
            if count == max_files:
                count = 0
                count_dir += 1
                out_dir = self.controller.saveDir.concat_dir(f'{max_files}-arquivos-{count_dir}')
                out_dir.mkdir()
            if file.path.exists():
                print(f'Movendo: {file.basename()} | {out_dir.basename()}')
                shutil.move(file.absolute(), out_dir.joinFile(file.basename()).absolute())
        
            #
        self.stop_pbar()
        self.label_progress.config(text='OK')
        print('OK')

    def set_size_screen(self):
        self.parent.title('Mover Arquvos')
        self.parent.geometry('440x180')

    def update_state(self):
        self.selected_files.clear()
        input_files = InputFiles(inputDir=self.controller.inputDir, maxFiles=4000)
        self.selected_files = input_files.getAllFiles()
        self._num_files = len(self.selected_files)
        self.label_top_info.config(text=f'Arquivos para Mover: {self._num_files}')
        self.label_info_out_dir.config(text=f'Pasta de destino: {self.controller.saveDir.basename()}')

    def start_pbar(self):
        self.running = True
        #self.pbar.start(10)

    def stop_pbar(self):
        self.running = False
        #self.pbar.stop()



#========================================================#
# Página selecionar ações.
#========================================================#
class PageSelectActions(AppPage):
    def __init__(self, *, parent, controller):
        super().__init__(parent=parent, controller=controller)
        self.page_name = '/home/select_actions'
        self.widgets = GetWidgets(self)
        self.frame_master = self.widgets.frame(self)
        self.frame_master.pack(padx=20, pady=20)
        self.frame_master.config(style='Custom.TFrame')
        self.padding=(8, 9)

        self.initUI()
        
    def initUI(self):
        self._start()
        # Botão voltar
        self.btn_back = self.widgets.get_button(self.frame_master)
        self.btn_back.config(text='Voltar', command=lambda: self.controller.navigatorPages.pop())
        self.btn_back.pack()
      
    def _start(self):
        
        #=========================================================#
        # Frame para botões Coordenadas.
        #=========================================================#
        self.frame_coordinates = self.widgets.frame(self.frame_master)
        self.frame_coordinates.pack(padx=3, pady=3, fill='both', expand=True,)
        # Label de topo
        self.label_top_coordinates = self.widgets.get_label(self.frame_coordinates)
        self.label_top_coordinates.config(text='Coordenadas')
        self.label_top_coordinates.pack()
        # Botão LatLon para UTM
        self.btn_latlon_to_utm = self.widgets.get_button(self.frame_coordinates)
        self.btn_latlon_to_utm.config(
            text='Latitude Longitude para UTM',
            padding=self.padding,
            command=(),
        )
        self.btn_latlon_to_utm.pack(padx=1, pady=3, fill='both', expand=True, side=tk.LEFT)   
        
        # Botão UTM para LatLon
        self.btn_utm_to_latlon = self.widgets.get_button(self.frame_coordinates)
        self.btn_utm_to_latlon.config(
            text='UTM para Latitude Longitude',
            padding=self.padding,
            command=lambda: self.controller.navigatorPages.push('/home/select_actions/to_latlon'),
        )
        self.btn_utm_to_latlon.pack(padx=1, pady=3, fill='both', expand=True, side=tk.LEFT)

        # Frame gerar mapa
        self.frame_maps = self.widgets.frame(self.frame_master)
        self.frame_maps.pack(padx=3, pady=3, fill='both', expand=True,)

        # Botão Gerar mapa
        self.btn_generate_map = self.widgets.get_button(self.frame_maps)
        self.btn_generate_map.config(
                text='Gerar Mapa',
                padding=self.padding,
                command=lambda: self.controller.navigatorPages.push('/home/select_actions/maps'),
            )
        self.btn_generate_map.pack(padx=1, pady=3, fill='both', expand=True, side=tk.LEFT)


        #----------------------------------------#
        # Frame para botões Excel
        #----------------------------------------#
        self.frame_buttons_excel = self.widgets.frame(self.frame_master)
        #self.frame_buttons_excel.config(style='Custom.TFrame')
        self.frame_buttons_excel.pack(padx=4, pady=4, fill='both', expand=True)
        # Label de topo do Frame
        self.label_text_info_excel = self.widgets.get_label(self.frame_buttons_excel)
        self.label_text_info_excel.config(text='Utilitários de Planilhas')
        self.label_text_info_excel.pack()
        
        # Botão planilhar pasta
        self.btn_folder_to_sheet = self.widgets.get_button(self.frame_buttons_excel)
        self.btn_folder_to_sheet.config(
                        text='Planilhar Pasta', 
                        padding=self.padding,
                        command=lambda: self.controller.to_page('/home/select_actions/folder_to_excel')
                    )
        self.btn_folder_to_sheet.pack(side=tk.LEFT, fill='both', expand=True)
        
        # Botão filtrar dados em Excel/CSV
        self.btn_filter_sheets = self.widgets.get_button(self.frame_buttons_excel)
        self.btn_filter_sheets.config(
                text='Filtrar Planilhas', 
                command=self.go_page_soup_sheets,
            )
        self.btn_filter_sheets.pack(side=tk.LEFT, fill='both', expand=True)

        #----------------------------------------#
        # Frame para botões Mover Arquivos
        #----------------------------------------#
        self.frame_buttons_move = self.widgets.frame(self.frame_master)
        self.frame_buttons_move.pack(padx=4, pady=4, fill='both', expand=True)
        # Botão Mover arquivos
        self.btn_move_files = self.widgets.get_button(self.frame_buttons_move)
        self.btn_move_files.config(
            text='Mover Arquivos', 
            command=lambda: self.controller.navigatorPages.push('/home/select_actions/move_files'),
        )
        self.btn_move_files.pack(padx=2, pady=2, fill='both', expand=True)

    def go_page_toi(self):
        files:List[File] = []
        if len(self.controller.inputFiles) < 1:
            # Usar arquivos de pasta
            if self.controller.inputDir is None:
                show_warnnings('Selecione arquivos para prosseguir!')
                return
            input_files = InputFiles(inputDir=self.controller.inputDir, maxFiles=4000)
            files = input_files.getAllFiles()
        else:
            # Usar arquivos selecionados pelo usuário.
            for f in self.controller.inputFiles:
                if f.is_pdf() or f.is_image():
                    files.append(File(f.absolute()))

        if len(files) < 1:
            show_warnnings('Selecione arquivos PDF/Imagens para prosseguir!')
            return
        
        self.controller.inputFiles = files
        self.controller.to_page('/home/select_actions/page_toi')

    def go_page_soup_sheets(self):
        files:List[FileSheet] = []
        if len(self.controller.inputFiles) < 1:
            # Usar arquivos de pasta
            if self.controller.inputDir is None:
                show_warnnings('Selecione arquivos Excel/CSV para prosseguir!')
                return
            input_files = InputFiles(inputDir=self.controller.inputDir, maxFiles=4000)
            files:List[FileSheet] = input_files.getFilesSheet()
        else:
            # Usar arquivos selecionados pelo usuário.
            for f in self.controller.inputFiles:
                if f.is_excel() or f.is_csv():
                    files.append(FileSheet(f.absolute()))

        if len(files) < 1:
            show_warnnings('Selecione arquivos Excel/CSV para prosseguir!')
            return
        self.controller.inputFiles = files
        self.controller.navigatorPages.push('/home/select_actions/soup_sheets')
    
    def set_size_screen(self):
        self.parent.geometry("510x300")
        self.parent.title(f"OCR Tool - Selecione uma ação")


class HomePage(AppPage):
    def __init__(self, *, parent:Tk, controller:ControllerApp):
        super().__init__(parent=parent, controller=controller)

        self.page_name = '/home'
        self.parent:Tk = parent
        self.controller:ControllerApp = controller
        self.home_widgets:GetWidgets = GetWidgets(self)
        
        # Frame Principal desta Tela
        self.style_frame = self.home_widgets.styleFrame
        self.frame_master:Frame = self.home_widgets.frame(self)
        self.frame_master.pack(padx=20, pady=20)
        self.frame_master.config(style='Custom.TFrame')
        self.initUI()

    def initUI(self):
        self.label_top_info = self.home_widgets.get_label(self.frame_master)
        self.label_top_info.config(text='Tela Inicial')
        self.label_top_info.pack()
        #----------------------------------------#
        # Frame Importar/Exportar
        #----------------------------------------#
        self.frame_import_export:ttk.Frame = self.home_widgets.frame(self.frame_master)
        self.frame_import_export.pack(padx=4, pady=4, fill='both', expand=True)
        # Label do topo
        self.label_import_export:ttk.Label = self.home_widgets.get_label(self.frame_import_export)
        self.label_import_export.config(text='Importar/Exportar Arquivos')
        self.label_import_export.pack()
        #----------------------------------------#
        # Frame Importar
        #----------------------------------------#
        self.frame_import = self.home_widgets.frame(self.frame_import_export)
        self.frame_import.pack(side=tk.LEFT, padx=3, pady=3)
        # Mostrar o total de arquivos selecionados.
        self.label_selected_num_files = self.home_widgets.get_label(self.frame_import)
        self.label_selected_num_files.config(
            text=f'Arquivos selecionados: {len(self.controller.inputFiles)}'
        )
        self.label_selected_num_files.pack()
        # Radios para selecionar o tipo de entrada de dados Arquivo/Pasta
        self.frame_radio_select_files = self.home_widgets.frame(self.frame_import)
        self.frame_radio_select_files.pack()
        self.radio_file_or_dir:tk.StringVar = tk.StringVar(value='from_file')
        # De Arquivo
        self.radio_opt_from_file = self.home_widgets.get_radio_button(self.frame_radio_select_files)
        self.radio_opt_from_file.config(
                        text='De Arquivo', 
                        variable=self.radio_file_or_dir,
                        value='from_file',
                    )
        self.radio_opt_from_file.pack(side=tk.LEFT)
        # De pasta
        self.radio_opt_from_dir = self.home_widgets.get_radio_button(self.frame_radio_select_files)
        self.radio_opt_from_dir.config(
                        text='De Pasta', 
                        variable=self.radio_file_or_dir,
                        value='from_dir',
                    )
        self.radio_opt_from_dir.pack(side=tk.LEFT)
        # Selecionar Arquivos
        self.btn_select_files:ttk.Button = self.home_widgets.get_button(self.frame_import)
        self.btn_select_files.config(text='Selecionar Arquivos', command=self.select_input_files)
        self.btn_select_files.pack()
        #----------------------------------------#
        # Frame Exportar
        #----------------------------------------#
        self.frame_export = self.home_widgets.frame(self.frame_import_export)
        self.frame_export.pack(padx=3, pady=3)
        # Mostrar o total de arquivos na pasta destino.
        self.label_num_files_outdir = self.home_widgets.get_label(self.frame_export)
        self.label_num_files_outdir.config(
            text=f'Arquivos no destino: {len(InputFiles(inputDir=self.controller.saveDir).getAllFiles())}'
        )
        self.label_num_files_outdir.pack()
        # Mostrar o diretório para exportar os dados
        self.label_export_dir = self.home_widgets.get_label(self.frame_export)
        self.label_export_dir.config(
            text=f'Exportar em: {self.controller.saveDir.basename()}'
        )
        self.label_export_dir.pack()
        # Botão para alterar a pasta de saída.
        self.btn_select_save_dir = self.home_widgets.get_button(self.frame_export)
        self.btn_select_save_dir.config(text='Alterar Pasta', command=self.select_outdir)
        self.btn_select_save_dir.pack()
       
        # botão avançar
        self.btn_next_page = self.home_widgets.get_button(self.frame_master)
        self.btn_next_page.config(command=self.go_page_select_actions, text='Avançar')
        self.btn_next_page.pack()

    def select_outdir(self):
        """Alterar a pasta para exportar os arquivo"""
        out = self._select_dir()
        if out is not None:
            self.controller.saveDir = Directory(out)
            self.label_export_dir.config(text=f'Exportar em: {self.controller.saveDir.basename()}')
            self.label_num_files_outdir.config(
                text=f'Arquivos no destino: {len(InputFiles(inputDir=self.controller.saveDir).getAllFiles())}'
            )

    def select_input_files(self):
        """
            Selecionar Arquivos ou diretórios, de acordo com a
        preferência do usuário.
        """
        self.controller.inputFiles.clear()

        if self.radio_file_or_dir.get() == 'from_file':
            f = self._select_file()
            if f is not None:
                self.controller.inputFiles.append(File(f))
                
        elif self.radio_file_or_dir.get() == 'from_dir':
            d = self._select_dir()
            if d is not None:
                self.controller.user_prefs.prefs['initial_dir'] = d
                self.controller.sharedValues.initialDir = d
                self.controller.inputDir = Directory(d)
                input_files = InputFiles(inputDir=self.controller.inputDir, maxFiles=5000)
                self.controller.inputFiles = input_files.getAllFiles()

        self.label_selected_num_files.config(
                            text=f'Arquivos selecionados: {len(self.controller.inputFiles)}'
                        )
        
    def _select_file(self) -> str | None:
        return self.controller.openFiles.open_filename()

    def _select_dir(self) -> str | None:
        return self.controller.openFiles.open_folder()

    def go_page_select_actions(self):
        if len(self.controller.inputFiles) < 1:
            show_warnnings('Selecione arquivos ou pasta para prosseguir!')
            return
        self.controller.navigatorPages.push('/home/select_actions')

    def set_size_screen(self):
        self.parent.geometry("460x205")
        self.parent.title("UTM Tools - Home")



class MyApplication(ControllerApp):
    def __init__(self, *, appDirs, sharedValues):
        super().__init__(appDirs=appDirs, sharedValues=sharedValues)

        self.root:Tk = self
        self.widgets:GetWidgets = GetWidgets(self)
        self.load_user_prefs()
        self.sharedValues.initialDir = self.user_prefs.prefs['initial_dir']
        
        # Gerenciar páginas
        self.controller:ControllerApp = self
        self.navigatorPages: Navigator = Navigator(parent=self, controller=self.controller)

        # Páginas
        _pages = (
            HomePage,
            PageSelectActions,
            PageFilesToExcel,
            PageSoupSheets,
            PageCreateMaps,
            PageConvertToLatLon,
            PageMoveFiles,
        )
        for page in _pages:
            self.navigatorPages.add_page(page)
        self.pages = self.navigatorPages.pages
        #
        self.initUI()
        self.last_frame:Frame = self.pages['/home']
        self.navigatorPages.push('/home')
        
    def initUI(self):
        self.title("Excel Tools")
        self.geometry("550x500")
        self.initMenuBar()

    def initMenuBar(self) -> None:
        
        # Criar o menu principal
        self.menu_bar = Menu(self.root)
        self.root.config(menu=self.menu_bar)

        self.create_menu_file()
        self.create_menu_config()
        self.create_menu_about()

    def create_menu_config(self):
        self.menu_config = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Configurações", menu=self.menu_config)
        # Incluir itens no menu configurações
        self.menu_config.add_command(
                                label=f"Arquivo de configuração:  ({self.appDirs.fileJson().absolute()})",
                                command=self.change_file_config,
                            )
        self.menu_config.index(tk.END)

    def create_menu_file(self):
        # Criar o menu Arquivo
        self.menu_file = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Arquivo", menu=self.menu_file)
        #
        self.cmd_go_back_page = self.add_item_menu_file(
            label='Voltar',
            tooltip='Voltar para a página anterior',
            command=lambda: self.navigatorPages.pop(),
        )

        #
        self.exit_cmd = self.add_item_menu_file(
            label='Sair', 
            tooltip='Sair do programa', 
            command=self.exit_app
        )

    def add_item_menu_file(self, label: str, tooltip: str, command: Callable[[], None]) -> int:
        """
        Adiciona um item ao menu 'Arquivo' com um tooltip.

        :param label: Nome do item no menu.
        :param tooltip: Texto do tooltip exibido no menu.
        :param command: Função a ser chamada ao clicar no item.
        :return: Índice do item adicionado no menu.
        """
        self.menu_file.add_command(
            label=f"{label} ({tooltip})",
            command=command,
        )
        return self.menu_file.index(tk.END)

    def select_bin_file(self, label: str) -> None:
        """
        Abre um diálogo para selecionar um arquivo e armazenar o caminho em uma variável.

        :param label: Rótulo do item no menu.
        """
        path_file: str = filedialog.askopenfilename(
            title=f"Selecione um arquivo para {label}",
            initialdir=self.sharedValues.initialDir,
        )
        if path_file:
            # Armazena o caminho do arquivo selecionado na variável correspondente
            if label == "tesseract":
                self.appDirs.path_bin_tesseract = File(path_file)
                self.menu_file.entryconfig(
                    self.tesseract_index, 
                    label=f"Tesseract: {path_file}"
                )
                self.user_prefs.prefs['path_tesseract'] = path_file
            elif label == "ocrmypdf":
                self.appDirs.path_bin_ocrmypdf = File(path_file)
                self.menu_file.entryconfig(
                    self.ocrmypdf_index, label=f"OCRMyPDF: {path_file}"
                )
                self.user_prefs.prefs['path_ocrmypdf'] = path_file
            messagebox.showinfo(label, f"Caminho selecionado:\n{path_file}")
        else:
            messagebox.showinfo(label, "Nenhum arquivo foi selecionado.")

    def create_menu_about(self) -> None:
        """Exibe informações sobre o programa."""
        # Adicionar o menu "Sobre" 
        self.autor = f'Autor: {__author__}'
        self.versao = f'Versão: {__version__} | Atualização {__update__}' 
        self.version_lib = f'Lib: {__version_lib__}'
        self.url = f'URL: {__url__}'

        self.menu_sobre = Menu(self.menu_bar, tearoff=0)
        self.menu_bar.add_cascade(label="Sobre", menu=self.menu_sobre)

        self.add_items_menu_about()
        self.root.config(menu=self.menu_bar)
    
    def add_items_menu_about(self):
        self.menu_sobre.add_command(label=self.autor,)
        self.menu_sobre.add_command(label=self.versao,)
        self.menu_sobre.add_command(label=self.version_lib)
        self.menu_sobre.add_command(label=self.url,)

    def get_current_tesseract(self) -> Optional[str]:
        """Retorna o caminho do arquivo selecionado para Tesseract."""
        return self.appDirs.fileTesseract().absolute()

    def get_current_ocrmypdf(self) -> Optional[str]:
        """Retorna o caminho do arquivo selecionado para OCRMyPDF."""
        return self.appDirs.fileOcrmypdf().absolute()

    def change_file_config(self) -> None:
        """
            Alterar o arquivo de configuração
        """
        filename:str = filedialog.askopenfilename(
            title=f"Selecione um arquivo para JSON",
            initialdir=self.sharedValues.initialDir,
            filetypes=[("Arquivos JSON", "*.json")]
        )
        if not filename:
            return
        if not os.path.isfile(filename):
            return
        
        self.appDirs._path_file_json = FileJson(filename)
        self.update_state()

    def update_menu_bar(self):
        self.menu_config.entryconfig(0,label=f'Arquivo: {self.appDirs.fileJson().absolute()}')
        self.sharedValues.initialDir = self.user_prefs.prefs['initial_dir']

    def update_state(self):
        self.load_user_prefs()
        self.update_menu_bar()

    def exit_app(self):
        # Salvar as configurações alteradas, antes de sair
        if os.path.isdir(self.sharedValues.initialDir):
            self.user_prefs.prefs['initial_dir'] = self.sharedValues.initialDir
        else:
            self.user_prefs.prefs['initial_dir'] = self.appDirs.ufs.dir_downloads.absolute()

        print(f'Salvando configurações em: [{self.appDirs.fileJson().absolute()}]')
        self.appDirs.fileJson().write(dataJson=self.user_prefs.prefs)
        print('OK')
        self.root.quit()

    def load_user_prefs(self) -> None:
        """
            Ler um arquivo de configuração local
        """
        f: FileJson = self.appDirs.fileJson()
        user_prefs:UserPrefs = self.appDirs.default_prefs()
        if not f.path.exists():
            # Salvar o arquivo de configuração localmente se
            # Se o arquivo ainda não existir
            print(f'Salvando configurações em: [{f.absolute()}]')
            f.write(dataJson=user_prefs.prefs)
            return 
        # Ler as configurações alteradas anteriormente.
        _data = f.toDict()
        try:
            if self.user_prefs.prefs['initial_dir'] is not None:
                self.sharedValues.initialDir = self.user_prefs.prefs['initial_dir']
            else:
                self.sharedValues.initialDir = self.appDirs.ufs.dir_downloads.absolute()
                self.user_prefs.prefs['initial_dir'] = self.sharedValues.initialDir
        except:
            self.sharedValues.initialDir = self.appDirs.ufs.dir_downloads.absolute()
            self.user_prefs.prefs['initial_dir'] = self.sharedValues.initialDir

        self.user_prefs.prefs = _data
        #print(f'\n{self.user_prefs.prefs}\n')
        return 



# Criação da janela principal e execução do aplicativo
def run_gui_ocr():
    
    appname='Document-Converter'
    appDirs:OcrAppDirs = OcrAppDirs(appname=appname)
    sharedValues=SharedValues()
    #app = AppMainWindow(root)
    app = MyApplication(
        appDirs=appDirs,
        sharedValues=sharedValues,
    )
    app.mainloop()


if __name__ == "__main__":
    run_gui_ocr()


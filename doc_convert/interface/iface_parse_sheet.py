#!/usr/bin/env python3
#

# External libs
from typing import (List, Dict)
from pandas import (
    DataFrame, 
    Series
)

class IFormatDataFrame(object):
    def __init__(self, *, data:DataFrame) -> None:
        self._data: DataFrame = data.astype('str')

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, n):
        self._data = n

    def header(self) -> List[str]:
        return self.data.columns.tolist()
    
    def indexList(self, *, column:str, text:str, iqual:bool=True) -> List[int]:
        pass

    def existsColumn(self, column:str, iqual:bool=True) -> bool:
        pass

    def existsColumns(self, *, columns:List[str]) -> bool:
        pass

    def getColumnElements(self, *, column:str, value:object, iqual:bool=True) -> Series:
        pass

    def getElements(self, *, column:str, value:object, iqual:bool=True) -> DataFrame:
        pass

    def concatColumns(self, *, columns:List[str], separator:str='_') -> None:
        pass

    def removeLines(self, *, column:str, value:object, iqual:bool=True) -> None:
        pass

    def removeNullLines(self, *, column:str) -> None:
        pass

    def deletColumn(self, *, column:str) -> None:
        pass

    def getColumnUniq(self, column:str) -> List[object]:
        pass



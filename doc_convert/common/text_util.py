#!/usr/bin/env python3
#

from __future__ import annotations
import re
from typing import (List, Dict, Tuple)
from pandas import Timestamp
import pandas
from datetime import datetime

from doc_convert.utils.util import (
    DataList, 
)

#===================================================================#
# Manipulação de Texto
#===================================================================#

def string_replace_regex(text:str) -> str:
    """
        Remove caractéres não UTF8 e substitui por espaço.
    """
    
    items_for_remove = [
                        '\xa0T\x04'
                    ]
    
    for i in items_for_remove:
        REG = re.compile(i)
        text = REG.sub(" ", text)
    return text

def string_to_uft8(text:str) -> str:
    """
        Converte string para string UTF8
    """
    # Codifica o texto como UTF-8, substitui caracteres inválidos por "?" 
    # e depois os converte para espaços
    cleaned_text = text.encode("utf-8", errors="replace").decode("utf-8")
    
    # Substitui "?" por espaços
    cleaned_text = cleaned_text.replace("?", " ")
    return string_replace_regex(cleaned_text)


def remove_chars(text:str) -> str:
    char_for_remove = [
                        ':', ',', ';', '$', 
                        '=', '!', '}', '{', '(', ')', 
                        '|', '\\', '‘', '*'
                        '¢', '“', '\'', '¢', '"', '#',
                    ]

    for char in char_for_remove:
        text = text.replace(char, '')
    return text.replace('/', '')


def replace_chars(text:str, *, new_char='-') -> str:
    char_for_remove = [
                        ':', ',', ';', '$', '=', 
                        '!', '}', '{', '(', ')', 
                        '|', '\\', '‘', '*'
                        '¢', '“', '\'', '¢', '"', 
                        '#', '.', '<', '?', '>', 
                        '»', '@', '+', '[', ']',
                        '%', '~', '¥',
               ]

    for char in char_for_remove:
        text = text.replace(char, new_char)
    return text


def is_valid_date(date_string, formats=('%Y-%m-%d', '%d/%m/%Y')):
  """
    Verifica se uma string pode ser convertida em uma data válida.

  Args:
    date_string: A string a ser verificada.
    formats: Uma tupla de formatos de data possíveis.

  Returns:
    True se a string puder ser convertida em uma data, False caso contrário.
  """

  for fmt in formats:
    try:
      datetime.datetime.strptime(date_string, fmt)
      return True
    except ValueError:
      pass
  return False


# Função para verificar se uma string pode ser convertida em data 
def is_date(string): 

    try: 
        pandas.to_datetime(string, errors='raise') 
        return True 
    except (ValueError, TypeError): 
        return False


class FormatString(object):
    def __init__(self, value:str) -> None:
        self.value = value
        self.__dt:datetime = None
        # ["%Y-%m-%d", "%d-%m-%Y", "%m/%d/%Y"]  # Adicione formatos suportados
        self.formats:Tuple[str]=(
                        '%Y/%m/%d',
                        '%d/%m/%Y', 
                        '%d/%m/%y',
                        '%Y-%m-%d',
                        '%d-%m-%y',
                        '%d-%m-%Y',
                        '%m/%d/%Y',
                        '%Y-%m-%d %H:%M:%S',
                    )

    def getDate(self) -> datetime | None:
        """
            Retorna um objeto datetime ou None.
        """
        if self.__dt is not None:
            return self.__dt
        dt = self.toDate()
        if dt is None:
            if self.isTimesTamp():
                pass
            return None
        self.__dt = datetime.strptime(self.value, '%d/%m/%Y')
        return self.__dt

    def setDate(self, d:datetime):
        self.__dt = d

    def toUtf8(self) -> FormatString:
        items_for_remove = [
                        '\xa0T\x04'
                    ]
        try:
            for i in items_for_remove:
                REG = re.compile(i)
                self.value = REG.sub("_", self.value)
        except:
            return self
        else:
            self.value = self.value.encode("utf-8", errors="replace").decode("utf-8")
        return self
    
    def replaceAll(self, char:str, new_char:str) -> FormatString:
        """
            Usar expressão regular para substituir caracteres.
        """
        self.value = re.sub(re.escape(char), "_", self.value)
        return self
    
    def replaceChars(self, *, new_char='-') -> FormatString:
        char_for_remove = [
                            ':', ',', ';', '$', '=', 
                            '!', '}', '{', '(', ')', 
                            '|', '\\', '‘', '*'
                            '¢', '“', '\'', '¢', '"', 
                            '#', '.', '<', '?', '>', 
                            '»', '@', '+', '[', ']',
                            '%', '~', '¥',
                ]

        for char in char_for_remove:
            self.value = self.value.replace(char, new_char)
        # Substituir outros caracteres
        self.value = re.sub(r'\s+', '_', self.value)
        format_chars = [
            '-_', '_-', '--', '__',
        ]
        for c in format_chars:
            self.replaceAll(c, '_')
        return self
    
    def toDate(self) -> str | None:
        """
            Verifica se uma string pode ser convertida em uma data válida.

        """
        d = None
        if isinstance(self.value, datetime):
            d = self.value.strftime("%d/%m/%Y")
        elif isinstance(self.value, (int, float)):  # Timestamp
            d = datetime.fromtimestamp(self.value).strftime("%d/%m/%Y")
        elif isinstance(self.value, str):  # String 
            for fmt in self.formats:
                try:
                    d = datetime.strptime(self.value, fmt).strftime("%d/%m/%Y")
                    break
                except ValueError:
                    continue
        if d is None:
            return None
        self.value = d
        return self.value
    
    def isValidDate(self) -> bool:
        if self.toDate() is None:
            return False
        return True
    
    def isTimesTamp(self) -> bool:
        """
            Verifica se o objeto atual é timestamp
        """
        if isinstance(self.value, Timestamp):
            self.setDate(datetime.fromtimestamp(self.value))
            return True
        return False

    def formatValue(self) -> FormatString:
        """Remove caracteres indesejados."""
        try:
            self.toUtf8().replaceChars()
        except Exception as e:
            print(f'\n{e}\n')
            return self
        else:
            return self

#===================================================================#
# Filtro de Texto
#===================================================================#
class IfaceFindElements(object):
    def __init__(self, *, text: str, corresp: str = None, separator:str=' ') -> None:
        self.corresp: str = corresp
        self.text: str = text
        self.separator:str = separator

    def regex(self):
        return re.compile(f'{self.text}.*{self.corresp}.*')
    
    def getLines(self, elements: List[str]) -> DataList:
        """
          -
        """
        values: List[object] = []
        REG = self.regex()
        for i in elements:
            values.extend(re.findall(self.regex(), i))
        return DataList(items=values)
    

class FindElements(IfaceFindElements):
    def __init__(self, *, text: str, corresp: str = None, separator: str = ' ') -> None:
        super().__init__(text=text, corresp=corresp, separator=separator)
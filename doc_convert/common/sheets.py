#!/usr/bin/env python3
#
from __future__ import annotations
import re
from typing import (List, Dict)
from pandas import (DataFrame, Series)
from openpyxl import Workbook, load_workbook
from openpyxl.cell.cell import Cell
from openpyxl.styles import PatternFill, Font

from doc_convert.utils.util import (
    std_util,
    File,
)

from doc_convert.interface.iface_parse_sheet import (
    IFormatDataFrame
)

def save_dataframe(*, data: DataFrame, outFile: File, index=False) -> bool:
    """Exporta um DataFrame para arquivo excel"""
    if not isinstance(outFile, File):
        std_util.e(f'outFile precisa ser do tipo File(), não {type(outFile)}')
        return False

    std_util.p(f'Exportando: {outFile.absolute()}')
    data.to_excel(outFile.absolute(), index=index)
    print('OK')
    return False

class FormatData(IFormatDataFrame):
    """
        Classe para formatar um DataFrame
    """
    def __init__(self, *, data: DataFrame) -> None:
        super().__init__(data=data)

    def header(self) -> List[str]:
        """Retorna o cabeçalho do DataFrame"""
        if self.data.empty:
            return []
        return self.data.columns.tolist()
    
    def indexList(self, *, column: str, text: str, iqual: bool = True) -> List[int]:
        """
            Filtra texto em uma coluna e retorna os respectivos índices.
        """
        # indices = df[df['texto'].str.contains(palavra, case=False, na=False)].index
        if not self.existsColumn(column, iqual=True):
            return []
        if iqual == False:
            return self.data[self.data[column].str.contains(text, case=False, na=False)].index.tolist()
        
        s: Series = self.data[column]
        index_list:List[int] = s[s == text].index.tolist()
        return index_list
        
    def existsColumn(self, column: str, iqual: bool = True) -> bool:
        _status = False
        if iqual == True:
            for col in self.header():
                if col == column:
                    _status = True
                    break
        else:
            for c in self.header():
                if column in c:
                    _status = True
                    break
        return _status
        
    def existsColumns(self, *, columns:List[str]) -> bool:
        _status = True
        for c in columns:
            if not self.existsColumn(c, iqual=True):
                _status = False
                break
        return _status
    
    def getColumnElements(self, *, column:str, value:object, iqual:bool=True) -> Series:
        """Filtra texto e retorna uma coluna do tipo Series()"""
        if not self.existsColumn(column):
            return Series()
        df = self.data[self.data[column].str.contains(value, case=False, na=False)]
        if df.empty:
            return Series()
        return df[column]

    def getElements(self, *, column:str, value:object, iqual:bool=True) -> DataFrame:
        """Filtra texto e retorna um novo DataFrame() filtrado."""
        if not self.existsColumn(column, iqual=True):
            return DataFrame()
        index_items: List[int] = self.indexList(column=column, text=value, iqual=iqual)
        if index_items == []:
            return DataFrame()
        if max(self.data.index.tolist()) < max(index_items):
            return DataFrame()
        return self.data.iloc[index_items]

    def concatColumns(self, *, columns:List[str], separator:str='_') -> None:
        """Concatena colunas no DataFrame Original"""
        # df['nova_coluna'] = df['col1'].astype(str) + '_' + df['col2'].astype(str) + '_' + df['col3'].astype(str)

        if not self.existsColumns(columns=columns):
            print(f'As colunas não existem: {columns}')
            return None
        
        # Criar uma lista vazia para armazenar os valores concatenados
        column_concat = []

        # Iterar sobre as linhas do DataFrame
        iter_rows = self.data.iterrows()
        for index, row in iter_rows:
            # Concatenar os valores das colunas selecionadas
            value_concat = separator.join([str(row[col]) for col in columns])
            column_concat.append(value_concat)

        # Adicionar a nova coluna ao DataFrame
        self.data['concatenar'] = column_concat

    def removeLines(self, *, column: str, value:str, iqual: bool = True) -> FormatData:
        
        # # Filtra as linhas que não correspondem ao padrão 
        # filtered_df = df[~df['coluna'].str.contains(pattern)]
        if not self.existsColumn(column):
            return None
        if iqual == True:
            self.data = self.data[self.data[column] != value]
            return self
        
        pattern = re.compile(r'{}'.format(value))
        self.data = self.data[~self.data[column].str.contains(pattern)]

    def removeNullLines(self, *, column:str) -> FormatData:
        if not self.existsColumn(column):
            return None
        self.data = self.data.dropna(subset=[column])
        self.data = self.data[self.data[column] != '']
        return self
    
    def deletColumn(self, *, column:str) -> None:
        if not self.existsColumn(column):
            return None
        self.data = self.data.drop(column, axis=1)

    def deletColuns(self, *, colums:List[str]) -> None:
        for c in colums:
            self.deletColumn(column=c)

    def getColumnUniq(self, column: str) -> List[object]:
        if not self.existsColumn(column, iqual=True):
            return []
        return self.data[column].unique().tolist()


class FormatFileExcel(object):
    """
        Ações difinidas para formatar um arquivo excel.
    """
    def __init__(self, f: File) -> None:
        self.file: File = f
        # Definindo a largura das colunas para aproximadamente 15 cm
        # 1 cm ≈ 5.67 pontos, então 15 cm ≈ 85 pontos
        self.LARGURA: int = 55
        # Fundo preto
        self.BG = PatternFill(start_color="000000", end_color="000000", fill_type="solid")
        # Fundo azul
        #self.BG = PatternFill(start_color="ADD8E6", end_color="ADD8E6", fill_type="solid")
        self.FONT = Font(color="FFFFFF")
        self._wb: Workbook = None

    def getWorkbook(self) -> Workbook:
        if self._wb is not None:
            return self._wb
        self._wb = load_workbook(self.file.absolute())
        return self._wb

    def format_space_colums(self):
        # Carregando o arquivo Excel com openpyxl para definir a largura das colunas
        std_util.p(f'Lendo arquivo: {self.file.absolute()}')
        ws = self.getWorkbook().active

        for col in ws.columns:
            max_length = 0
            col_letter = col[0].column_letter  # Identificador da coluna (A, B, C, ...)
            ws.column_dimensions[col_letter].width = self.LARGURA
    
    def format_header(self) -> None:

        ws = self.getWorkbook().active[1]
        # Aplicar o estilo às células do cabeçalho (primeira linha)
        for cell in ws:
            print(type(cell))
            cell.fill = self.BG
            cell.font = self.FONT
        
    def save(self):
        # Salvando o arquivo Excel com as alterações
        std_util.p(f'Salvando alterações: {self.file.absolute()}')
        self.getWorkbook().save(self.file.absolute())


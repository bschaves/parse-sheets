#!/usr/bin/env python3
#

from __future__ import annotations
import os
import sys
import platform
import json
from enum import Enum
import urllib
import urllib.request
from pathlib import Path
from time import sleep
from typing import List, Dict, Self


# Status da ultima operação em forma de texto.
current_output: str = None
current_status: bool = False
KERNEL_TYPE:str = platform.system()

def global_sleep():
    sleep(0.2)


class ByteSize(int):
    """
      Classe para mostrar o tamaho de um arquivo (B, KB, MB, GB) de modo legível para humanos.
    """

    # https://qastack.com.br/programming/1392413/calculating-a-directorys-size-using-python
    # 2021-11-13 - 21:12
    
    _kB = 1024
    _suffixes = 'B', 'kB', 'MB', 'GB', 'PB'

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        self.bytes = self.B = int(self)
        self.kilobytes = self.kB = self / self._kB**1
        self.megabytes = self.MB = self / self._kB**2
        self.gigabytes = self.GB = self / self._kB**3
        self.petabytes = self.PB = self / self._kB**4
        *suffixes, last = self._suffixes
        suffix = next((
            suffix
            for suffix in suffixes
            if 1 < getattr(self, suffix) < self._kB
        ), last)
        self.readable = suffix, getattr(self, suffix)

        super().__init__()

    def __str__(self):
        return self.__format__('.2f')

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, super().__repr__())

    def __format__(self, format_spec):
        suffix, val = self.readable
        return '{val:{fmt}} {suf}'.format(val=val, fmt=format_spec, suf=suffix)

    def __sub__(self, other):
        return self.__class__(super().__sub__(other))

    def __add__(self, other):
        return self.__class__(super().__add__(other))

    def __mul__(self, other):
        return self.__class__(super().__mul__(other))

    def __rsub__(self, other):
        return self.__class__(super().__sub__(other))

    def __radd__(self, other):
        return self.__class__(super().__add__(other))

    def __rmul__(self, other):
        return self.__class__(super().__rmul__(other))

class StatusOutput(object):
    def __init__(self):
        self.__current_status: bool = False
        self.__current_output: str = None

    def get_output(self) -> str:
        return self.__current_output

    def get_status(self) -> bool:
        return self.__current_status

    def update(self, *, output: str, status:bool):
        self.__current_output = output
        self.__current_status = status

    def update_output(self, output: str):
        self.__current_output = output

    def update_status(self, status:bool):
        self.__current_status = status

status_output_info: StatusOutput = StatusOutput()

class StdoutUtils(object):
    def __init__(self, *, silent: bool=False) -> None:
        self._silent:bool = silent

    @property
    def silent(self):
        return self._silent
    
    @silent.setter
    def silent(self, new_silent: bool):
        self._silent = new_silent

    def get_terminal_colums(self) -> int:
        try:
            return os.get_terminal_size()[0]
        except:
            return 80
        
    def p(self, text):
        if self.silent:
            return 
        print(f'[+] {text}')

    def print_line(self, char:str='-'):
        if self.silent:
            return
        print(char * self.get_terminal_colums())

    def e(self, text:str):
        if self.silent:
            return
        self.print_line()
        print(f'[!] Erro: {text}')

std_util = StdoutUtils()

def question(text: str) -> bool:
    """
    :param text: Exibir um texto para o usuário em forma de indagação. 
                 EX: Deseja prosseguir? O ponto de interrogação e colocado 
                 automaticamente por esta função.

    :return: Se o usuário responder s/y com o teclado retorna True, se não retorna False.
    """
    try:
        yesno = str(input(f'{text}? [s/N]: '))
    except(KeyboardInterrupt):
        print('Abortando...')
        return False
    except Exception as e:
        print(e)
        return False
    else:
        if (yesno.lower() == 's') or (yesno.lower() == 'y'):
            return True
        return False

def get_abspath(path: str) -> str:
    return os.path.abspath(path)

def mkdir(path: str) -> None:
    try:
        os.makedirs(path)
    except Exception as e:
        current_output = e
        current_status = False

        status_output_info.update(
            output=e,
            status=False
        )
    else:
        status_output_info.update_status(True)


def get_user_home_dir():
    return get_abspath(Path().home())


def remove_new_line_in_list(list_of_strings: List[str], char:str=' ') -> list:
    """
        Apaga as quebras de linhas de uma lista, e substitui as quebras de linhas por outro caracter
    em seguida, retorna um nova lista.

    O caracter padrão é um ' '.
    """
    finaList: List[str] = []
    newLine = '\n'

    if not isinstance(list_of_strings, list):
            raise Exception(f'{list_of_strings} -> não é uma do tipo lista')

    for element in list_of_strings:
        try:
            finaList.append(element.replace(newLine, char))
        except Exception as e:
            print(e)
            global_sleep()
    return finaList

def is_iterable(item) -> bool:
        try: 
            iter(item) 
            return True 
        except TypeError: 
            return False

#======================================================#
# Objetos para trabalhar com ARQUIVOS
#======================================================#
class IfaceFilePath(object):
    def __init__(self, filename: str):
        self.filename = filename
        self.path: Path = Path(self.filename)

    def dirname(self):
        return os.path.dirname(self.absolute())

    def basename(self):
        return os.path.basename(self.absolute())

    def absolute(self):
        return get_abspath(self.filename)
    
    def name(self) -> str:
        return self.basename().replace(self.extension(), '')
    
    def nameAbsolute(self) -> str:
        return self.absolute().replace(self.extension(), '')

    def extension(self) -> str:
        return self.path.suffix
    
    def size(self) -> int:
        if not self.path.exists():
            return 0
        return self.path.stat().st_size

    def formatedSize(self) -> ByteSize:
        return ByteSize(self.size())
    
    def human_size(self) -> str:
        if self.size() < 1000:
            return f'{self.size()} B'
        elif self.size() < 1000000:
            return f'{(self.size()/1000):.2f} KB'
        elif self.size() < 1000000000:
            return f'{(self.size()/1000000):.2f} MB'
        else:
            return f'{(self.size()/1000000000):.2f} GB'
        
    

class File(IfaceFilePath):
    def __init__(self, filename: str):
        super().__init__(filename)

    def is_pdf(self) -> bool:
        try:
            if self.extension().lower() == '.pdf':
                return True
            return False
        except:
            return False

    def is_image(self) -> bool:
        try:
            e = self.extension().lower()
            if (e == '.png') or (e == '.jpeg') or (e == '.jpg') or (e == '.svg'):
                return True
            return False
        except:
            return False
        
    def is_excel(self) -> bool:
        try:
            e = self.extension().lower()
            if (e == '.xls') or (e == '.xlsx'):
                return True
            return False
        except:
            return False
        
    def is_csv(self) -> bool:
        try:
            e = self.extension().lower()
            if (e == '.csv'):
                return True
            return False
        except:
            return False
        
    def is_txt(self) -> bool:
        try:
            e = self.extension().lower()
        except:
            return False
        else:
            if (e == '.txt'):
                return True
            return False
        
    def updateName(self, name:str) -> File:
        """
            Altera o nome do arquivo, incluindo a extensão
        para um novo nome no diretório atual.
        """
        return File(
            os.path.join(self.dirname(), name)
        )

#======================================================#
# Objetos para trabalhar com DIRETÓRIOS
#======================================================#
class _dpath(object):
    def __init__(self, pathDir:str, *, create=False):
        self.path_dir: str = pathDir
        if not isinstance(pathDir, str):
            print(f'{__class__.__name__}: path_dir precisa ser do tipo str não {type(pathDir)}')
            sys.exit(1)

        self.create: bool = create
        self._current_status: bool = False
        self._current_output: str = None
        self.path: Path = Path(self.path_dir)

        if create:
            self.mkdir()

    def absolute(self) -> str:
        return get_abspath(self.path_dir)
    
    def basename(self) -> str:
        return os.path.basename(self.absolute())
    
    def dirname(self) -> str:
        return os.path.dirname(self.absolute())

    def joinFile(self, fname:str) -> File:
        if not isinstance(fname, str):
            print(f'{__class__.__name__} fname precisa ser do tipo str não {type(fname)}')
            sys.exit(1)
        return File(os.path.join(self.absolute(), fname))

    def mkdir(self):
        try:
            os.makedirs(self.absolute())
        except:
            pass
    
    def getContentFiles(self, *, content_in_file: str='*') -> List[File]:
        """
            Retorna os arquivos nos diretórios e subdiretórios.
        """
        _files = self.path.rglob(content_in_file)
        _new_list = []
        f: Path = None
        for f in _files:
            if f is None:
                break

            if f.is_file():
                _new_list.append(File(get_abspath(f.absolute())))
        return _new_list

    def concatPath(self, new_dir:str) -> Path:
        return Path(os.path.join(self.absolute(), new_dir))
    

class dirpath(_dpath):
    def __init__(self, path_dir: str, *, create=False):
        super().__init__(path_dir, create=create)

    
class Directory(dirpath):
    def __init__(self, path_dir: str, *, create=False):
        super().__init__(path_dir, create=create)

    def concat_dir(self, new_dir: str) -> Directory:
        #return dirpath(os.path.join(self.absolute(), new_dir))
        return Directory(os.path.join(self.absolute(), new_dir))

    def parent(self) -> Directory:
        return Directory(self.dirname())
 


class TypeText(Enum):
    STRING = 'string'
    LIST = 'list'
    DICT = 'dict'

      
class IfaceDataList(object):
    def __init__(self, *, items: List[str], separator=' ') -> None:
        if not isinstance(items, list):
            print(f'{__class__.__name__} self.items precisa ser do tipo list não {type(items)}')
            sys.exit(1)

        self._items: List[str] = items
        self.len: int = len(self._items)
        self.separator: str = separator

    @property
    def items(self):
        return self._items
    
    @items.setter
    def items(self, new_data:List[str]):
        if not isinstance(list, new_data):
            return
        self._items = new_data
        self.len = len(new_data)

    def length(self) -> int:
        return self.len

    def toString(self) -> str:
        pass

    def replaceItems(self, item:str, newItem:str) -> None:
        """Substituir em todos os itens da lista [item] por [newItem]"""
        pass

    def contains(self, text:str) -> bool:
        pass

    def containsIqual(self, text:str) -> bool:
        pass

    def findAndGetContains(self, text:str) -> List[str]:
        """Retorna todos os itens da lista que contanham [text]"""
        pass

    def getNextContains(self, text:str) -> str | None:
        """Ao encontrar [text] será retornado o próximo item da lista ou None"""
        pass

    def getNextAll(self, text:str) -> List[str]:
        """Ao encontar [text] na lista, será retornado todos os itens seguintes."""
        pass

    def getBackContains(self, text:str) -> str:
        pass

    def forItem(self, func) -> None:
        for i in self.items:
            func(i)

    def toUpper(self):
        pass

    def toLower(self):
        pass

    def deleteNewLine(self):
        pass

    def concatList(self, elements:List[str]):
        pass

    def indexIqual(self, text: str, *, ignore_case:bool=False) -> int:
        """
            Retorna uma lista de indices que contém a string text
        na lista.
        """
        pass
    
    def indexContains(self, text:str, *, ignore_case=False) -> List[int]:
        """
            Retorna uma lista de indices que contém a string text
        na lista.
        """
        pass

    def isEmpty(self) -> bool:
        pass

    def addItem(self, item:str):
        pass
    
    def addItems(self, items:List[str]):
        pass


class DataList(IfaceDataList):
    def __init__(self, *, items: List[str], separator=' ') -> None:
        super().__init__(items=items, separator=separator)

    def isEmpty(self) -> bool:
        if (self.items == []) or (len(self.items) <= 0):
            return True
        return False

    def toString(self) -> str:
        try:
            return self.separator.join(self.items)
        except:
            s = ''
            for num, i in enumerate(self.items):
                if num == 0:
                    s = i
                else:
                    s = f'{s}{self.separator}{i}'
            return s

    def replaceItems(self, item:str, newItem:str) -> Self:
        """Substituir em todos os itens da lista [item] por [newItem]"""
        for n in range(0, self.len):
            self.items[n] = self.items[n].replace(item, newItem)
        return self

    def contains(self, text:str) -> bool:
        _status = False
        for i in self.items:
            if text in i:
                _status = True
                break
        return _status

    def containsIqual(self, text:str) -> bool:
        _status = False
        for i in self.items:
            if text == i:
                _status = True
                break
        return _status

    def findAndGetContains(self, text:str) -> List[str]:
        """Retorna todos os itens da lista que contanham [text]"""
        newItems: List[str] = []
        for i in self.items:
            if text in i:
                newItems.append(i)
        return newItems

    def getNextContains(self, text:str) -> str | None:
        """Ao encontrar [text] será retornado o próximo item da lista ou None"""
        element:str = None
        for n in range(0, self.len):
            if text in self.items[n]:
                if n < self.len:
                    element = self.items[n+1]
                    break
        return element

    def getNextAll(self, text:str) -> List[str]:
        """Ao encontar [text] na lista, será retornado todos os itens seguintes."""
        num:int = None
        for n in range(0, self.len):
            if text in self.items[n]:
                if n < self.len:
                    num = n+1
                    break
        if num is None:
            return []
        return self.items[num:]

    def getBackContains(self, text:str) -> str:
        element:str = None
        for n in range(0, self.len):
            if text in self.items[n]:
                if n > 0:
                    element = self.items[n-1]
                    break
        return element

    def forItem(self, func) -> None:
        for i in self.items:
            func(i)

    def toUpper(self):
        for n in range(0, self.len):
            self.items[n] = self.items[n].upper()

    def toLower(self):
        for n in range(0, self.len):
            self.items[n] = self.items[n].lower()

    def deleteNewLine(self):
        for n in range(0, self.len):
            self.items[n] = self.items[n].replace('\n', '')

    def indexIqual(self, text: str, *, ignore_case:bool=False) -> int:
        """
            Retorna uma lista de indices que contém a string text
        na lista.
        """
        numbers: List[int] = []
        for n, i in enumerate(self.items):
            if ignore_case:
                if text.upper() == i.upper():
                    numbers.append(n)
            else:
                if text == i:
                    numbers.append(n)
        return numbers
    
    def indexContains(self, text:str, *, ignore_case=False) -> List[int]:
        """
            Retorna uma lista de indices que contém a string text
        na lista.
        """
        numbers: List[int] = []
        for n, i in enumerate(self.items):
            if ignore_case:
                if text.upper() in i.upper():
                    numbers.append(n)
            else:
                if text in i:
                    numbers.append(n)
        return numbers

    def concatList(self, elements: List[str], *, union:str='_', replaceNull:str='-') -> List[str]:
        """Concatena a lista atual com uma nova lista"""
        if len(elements) < 1:
            return []
        if self.length() < 1:
            return []
        
        concat:List[str] = []
        maxElements = len(elements)
        if self.len >= maxElements:
            for num, i in enumerate(self.items):
                if num >= maxElements:
                    # Concatenar os items da lista original com o valor padrão
                    concat.append(f'{i}{union}{replaceNull}')
                else:
                    concat.append(
                        f'{i}{union}{elements[num]}'
                    )
        else:
            for num_element, e in enumerate(elements):
                if num_element >= self.len:
                    concat.append(f'{replaceNull}{union}{e}')
                else:
                    concat.append(f'{self.items[num_element]}{union}{e}')
        return concat
    
    def addItem(self, item: str):
        self.items.append(item)
    
    def addItems(self, items: List[str]):
        self.items.extend(items)


class data_text(object):
    def __init__(self, textData:str, *, separator:str=' ') -> None:
        self.separator:str = separator
        self.textData:str = textData
        self.len: int = 0
        if self.textData is not None:
            self.len = len(self.textData)

        if isinstance(self.textData, str):
            print(f'{__class__.__name__} self.data precisa ser do tipo str, não {type(textData)}')
            
    def toString(self) -> str:
        if isinstance(str, self.textData):
            return self.textData
        return str(self.textData)
        
    def toList(self) -> List[str]:
        return self.textData.split(self.separator)

    def toDataList(self) -> DataList:
        pass

    def contains(self, text:str) -> bool:
        pass

    def isIqual(self, text:str) -> bool:
        pass

    def isString(self) -> bool:
        pass

    def isInterable(self) -> bool:
        pass


class DataText(data_text):
    def __init__(self, textData: str, *, separator: str = ' ') -> None:
        super().__init__(textData, separator=separator)

    def toDataList(self) -> DataList:
        return DataList(items=self.toList())

    def contains(self, text:str) -> bool:
        if text in self.toString():
            return True
        return False

    def isIqual(self, text:str) -> bool:
        if self.isString():
            if text == self.textData:
                return True
        elif self.isList():
            if text == self.toString():
                return True
        return False
    
    def isString(self) -> bool:
        if isinstance(self.textData, str):
            return True
        return False

    def isInterable(self) -> bool:
        try: 
            iter(self.textData) 
            return True 
        except TypeError: 
            return False
    

class DataJson(object):
    def __init__(self, dataJson:object) -> None:
        self.dataJson: object = dataJson

        if isinstance(self.dataJson, str):
            self.type_json = 'string'
        elif isinstance(self.dataJson, dict):
            self.type_json = 'dict'
        else:
            raise Exception(f'{__class__.__name__} -> data_json deve ser do tipo dict ou str')
    
    def isString(self) -> bool:
        return isinstance(self.dataJson, str)

    def isDict(self) -> bool:
        return isinstance(self.dataJson, dict)

    def toString(self) -> str:
        """
            Converter self.data_json para string
        """
        if self.isString():
            return self.dataJson
        if self.dataJson == {}:
            return ''
        if self.isDict():
            return json.dumps(self.dataJson, indent=4, ensure_ascii=False,)    
        return ''
    
    def toDict(self) -> dict:
        if self.isDict():
            return self.dataJson
        if self.dataJson == '':
            return {}
        if self.isString():
            return json.loads(self.dataJson)
        return {}
        
    def keys(self) -> List[object]:
        return list(self.toDict().keys())
    
    def values(self) -> List[object]:
        return list(self.toDict().values())
    
    def toDataList(self, *, separator=' ') -> DataList:
        return DataList(items=self.values(), separator=separator)
    

class ConvertJson(object):
    def __init__(self) -> None:
        pass

    def fromString(self, *, stringJson:str) -> DataJson:
        """Recebe uma string Python e converte em DataJson"""
        _data: object = json.loads(stringJson)
        return DataJson(_data)

    def fromDict(self, *, dictJson:Dict[str, object]) -> DataJson:
        json_string = json.dumps(dictJson, indent=4, ensure_ascii=False)
        return DataJson(json_string)

    def fromList(self, l: List[object]) -> List[DataJson]:
        listData: List[DataJson] = []
        for i in l:
            if (isinstance(i, str)) or (isinstance(i, dict)):
                listData.append(DataJson(i))
        return listData
    

class UserFileSystem(object):
    def __init__(self) -> None: 
        self._dir_home: Directory = Directory(get_user_home_dir())
        self.dir_downloads:Directory = self._dir_home.concat_dir('Downloads')
        self.dir_bin: Directory = self._dir_home.concat_dir('bin')

    @property
    def dir_home(self) -> Directory:
        return self._dir_home
    
    @property
    def dir_home(self, value:Directory):
        if not isinstance(value, Directory):
            print(f'{__class__.__name__} dir_home precisa ser do tipo Directory não {type(value)}')
            return
        self._dir_home = value
        self.dir_downloads = self._dir_home.concat_dir('Downloads')
        self.dir_bin = self._dir_home.concat_dir('bin')

    def dirUserDocuments(self) -> Directory:
        """Diretório documentos do usuário."""
        if self._dir_home.concat_dir('Documents').path.exists():
            return self._dir_home.concat_dir('Documents')
        elif self._dir_home.concat_dir('Documentos').path.exists():
            return self._dir_home.concat_dir('Documentos')
        else:
            _d = self._dir_home.concat_dir('Documentos')
            _d.mkdir()
            return _d
        
    def getDirVar(self) -> Directory:
        """Diretório para guardar arquivos e pastas diversas"""
        d = self._dir_home.concat_dir('var')
        d.mkdir()
        return d
    
    def getDirCache(self) -> Directory:
        d = self._dir_home.concat_dir('cache')
        d.mkdir()
        return d
          
    def getDirOpt(self) -> Directory:
        """
            Diretório OPT local
        """
        d = self.getDirVar().concat_dir('opt')
        d.mkdir()
        return d
    
    def getDirConfig(self) -> Directory:
        d = self._dir_home.concat_dir('.config')
        d.mkdir()
        return d
        
    def createDirs(self):
        self._dir_home.mkdir()
        self.dir_downloads.mkdir()
        self.dir_bin.mkdir()


class AppDirs(object):
    def __init__(self, *, appname:str, ufs: UserFileSystem = UserFileSystem()) -> None:
        self.ufs: UserFileSystem = ufs
        self.appname: str = appname

    def dirCache(self) -> Directory:
        d = self.ufs.getDirCache().concat_dir(self.appname)
        d.mkdir()
        return d

    def dirConfig(self) -> Directory:
        d = self.ufs.getDirConfig().concat_dir(self.appname)
        d.mkdir()
        return d

    def dirOpt(self) -> Directory:
        d = self.ufs.getDirOpt().concat_dir(self.appname)
        d.mkdir()
        return d

    def dirUserDocuments(self) -> Directory:
        return self.ufs.dirUserDocuments()

    def dirUserHome(self) -> Directory:
        d = get_abspath(Path().home())
        return Directory(d)
    
    def createDirs(self):
        self.dirCache().mkdir()
        self.dirConfig().mkdir()
        self.dirOpt().mkdir()


def download_file(*, url:str, file_path:File) -> bool:

    if file_path.path.exists():
        print(f'[PULANDO] o arquivo já existe: {file_path.basename()}')
        return True
    if not file_path.path.parent.exists():
        print(f'O diretório não existe: {get_abspath(file_path.path.parent.absolute())}')
        return False
    
    print(f'[BAIXANDO]: {file_path.basename()}')
    print(f'[URL]: {url}')
    try:
        urllib.request.urlretrieve(url, file_path.absolute())
    except Exception as e:
        print(f'{__name__} {e}\n')
        return False
    else:
        return True
    

def main():
    pass

if __name__ == '__main__':
    main()
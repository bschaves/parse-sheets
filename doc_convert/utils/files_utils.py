#!/usr/bin/env python3
#

import sys
import os
import json
import time
from pathlib import Path
from threading import (Thread, Event)
from typing import (List, Dict, Tuple)

import pandas
from pandas import DataFrame
from openpyxl import load_workbook, Workbook
from openpyxl.worksheet._read_only import ReadOnlyWorksheet

from PyPDF2 import (PageObject, PdfReader)

# Internal LIBS
from doc_convert.utils.util import (
    IfaceFilePath,
    Directory,
    File,
    DataText,
    DataList,
    is_iterable,
    std_util,
    KERNEL_TYPE,
)


#===================================================================#
# Modelos de arquivos no disco
#===================================================================#
       
class FileSheet(File):
    """
        Representação de um arquivo CSV/Excel
    """
    def __init__(self, filename: str):
        super().__init__(filename)
        if (not self.is_excel()) and (not self.is_csv()):
            raise Exception(f'{__class__.__name__} erro o arquivo precisa ser .csv ou .xlsx não {self.filename}')
        
    def data(self) -> DataFrame:
        if self.is_csv():
            return pandas.read_csv(self.filename)
        elif self.is_excel():
            return pandas.read_excel(self.filename)
  

class LoadSheet(object):
    def __init__(self, *, sheet:FileSheet):
        self.fileSheet:FileSheet = sheet
        self.listData:List[DataFrame] = []
        self.data:DataFrame = None
        self._num_rows:int = None
        self.__origin_header = None
        self.currentText:str = None
        self.currentProgress:float = 0
        self.running = False
        self.running_df = False

    def setNumRows(self, num):
        self._num_rows = num

    def getNumRows(self) -> int:
        """Número total de linhas da planilha"""
        return self._num_rows
    
    def getHeader(self) -> Tuple[str]:
        # Cabeçalho original da planilha
        pass
    
    def getNumColumns(self) -> int:
        pass

    def load(self):
        pass

    def setValues(self) -> bool:
        """Inicializa alguns valores da planilha"""
        pass

    def getData(self) -> DataFrame:
        pass


class LoadExcel(LoadSheet):
    """Ler o obter informações sobre um arquivo Excel."""
    def __init__(self, *, sheet:FileSheet):
        super().__init__(sheet=sheet)
        self.__read_only_sheet:ReadOnlyWorksheet = None
        # Cabeçalho original da planilha.
        self.__origin_header:Tuple[str] = None
        self.__set_values:bool = False
        
    def setHeader(self, h:Tuple[str]):
        self.__origin_header = h

    def getHeader(self) -> Tuple[str]:
        """Cabeçalho original da planilha"""
        if self.__origin_header is None:
            self.__origin_header = next(self.getReadOnlyWs().iter_rows(values_only=True))
        return self.__origin_header
    
    def setData(self, d:DataFrame):
        self.data = d

    def getNumRows(self):
        if self._num_rows is None:
            self.currentText = f'Calculando número de linhas: {self.fileSheet.basename()}'
            n = self.getReadOnlyWs().max_row
            self.setNumRows(n)
        return self._num_rows
    
    def _getReadOnlyWb(self) -> Workbook | None:
        self.currentText = f'Lendo:\n{self.fileSheet.basename()}'
        try:
            return load_workbook(self.fileSheet.absolute(), read_only=True)
        except Exception as e:
            print()
            print(f'{__class__.__name__}\n{e}\n')
            self.currentText = f'Erro ao tentar ler o arquivo: {self.fileSheet.basename()}'
            self.running = False
            return None

    def getReadOnlyWs(self) -> ReadOnlyWorksheet | None:
        """Objeto para obter algumas propriedades da planilha."""
        if self.__read_only_sheet is not None:
            return self.__read_only_sheet
        if self._getReadOnlyWb() is None:
            return None
        self.__read_only_sheet = self._getReadOnlyWb().active
        self.currentText = None
        return self.__read_only_sheet
    
    def _start_values(self):
        self.running = True
        self.currentText = f'Analizando a planilha: {self.fileSheet.basename()}'
        n = self.getReadOnlyWs().max_row
        self.setNumRows(n)
        self.currentText = None
        self.running = False
        self.__set_values = True

    def setValues(self):
        """
            Carrega os valores da planilha em segundo plano usando Thread.
        """
        if self.__set_values == True:
            return
        t = Thread(target=self._start_values)
        t.start()

    def load(self):
        """
            Carregar alguns valores da planilha e atualizar o 
        progresso durante a operação.
        """
        self.running_df = True
        self.running = True
        self.data = None
        self.listData.clear()
        currentData:List[tuple] = []
        if self.getReadOnlyWs() is None:
            self.running = False
            self.running_df = False
            return None
        _rows = self.getReadOnlyWs().iter_rows(values_only=True)
        for num, row in enumerate(_rows):
            if num == 0:
                # Ignorar a primeira linha (cabeçalho).
                continue
            currentData.append(row)
            self.currentProgress = (num/self.getNumRows()) * 100
            self.currentText = f'Lendo planilha: {self.fileSheet.basename()} [{self.fileSheet.human_size()}]'

        self.currentProgress = 100
        if currentData:
            df = DataFrame(currentData)
            if not df.empty:
                self.listData.append(df)
        # Excluir entradas vazias ou totalmente NA antes de concatenar 
        # listChunks = [df for df in listChunks if not df.isnull().all().all()]
        self.data = pandas.concat(self.listData, ignore_index=True)
        self.data.columns = self.getHeader()
        self.running = False
        self.running_df = False

    def getData(self) -> DataFrame:
        """
            Retorna o DataFrame da planilha.
        """
        if self.data is not None:
            self.running = False
            self.running_df = False
            return self.data
        self.load()
        return self.data if self.data is not None else DataFrame()
    

class LoadCsv(LoadSheet):
    def __init__(self, *, sheet, separator='\t'):
        super().__init__(sheet=sheet)
        self.separator = separator
        self.__set_values= False
        self.__running_df = False
        self.setValues()

    def setNumRows(self, num):
        self._num_rows = num

    def getNumRows(self):
        """Retornar o total de linhas da planilha CSV"""
        if self._num_rows is not None:
            return self._num_rows
        with open(self.fileSheet.absolute(), 'r', encoding='utf-8') as file:
            self._num_rows = sum(1 for _ in file)
        return self._num_rows
    
    def getHeader(self) -> Tuple[str]:
        """Cabeçalho original da planilha"""
        if self.__origin_header is not None:
            return self.__origin_header
        self.__origin_header = self.getData().columns.tolist()
        return self.__origin_header

    def _start_values(self):
        self.running = True
        self.currentText = f'Analizando a planilha: {self.fileSheet.basename()}'
        self.setNumRows(self.getNumRows())
        self.currentText = None
        self.running = False
        self.__set_values = True

    def setValues(self):
        """
            Carrega os valores da planilha em segundo plano usando Thread.
        """
        if self.__set_values == True:
            return
        t = Thread(target=self._start_values)
        t.start()
    
    def _load_csv_linux(self):
        self.running = True
        self.data = None
        num = self.getNumRows()
        elements:List[object] = []
        std_util.print_line()
        for chunk in pandas.read_csv(
                            self.fileSheet.absolute(), 
                            chunksize=1000, 
                            encoding='utf-8', 
                            delimiter=self.separator, 
                        ):
            elements.append(chunk)
            self.currentProgress = len(chunk)/num
            self.currentText = f'Lendo: {self.fileSheet.basename()} | {self.fileSheet.human_size()}'
                
        self.currentProgress = 100
        self.data = pandas.concat(elements, ignore_index=True)
        self.__origin_header = self.data.columns.tolist()
        self.running = False
        self.running_df = False

    def _load_csv_windows(self) -> None:
        std_util.print_line()
        std_util.p(f'Lendo CSV {KERNEL_TYPE}\n\n')
        self.running = True
        self.data = None
        self.currentProgress = 0
        self.currentText = f'Lendo: {self.fileSheet.basename()} | {self.fileSheet.human_size()}'
        self.data = pandas.read_csv(
            self.fileSheet.absolute(), 
            encoding='utf-8', 
            sep=self.separator,
        )
        #
        self.__origin_header = self.data.columns.tolist()
        self.currentProgress = 100
        self.running = False
        self.running_df = False

    def load(self):
        if  KERNEL_TYPE == 'Windows':
            self._load_csv_windows()
        else:
            self._load_csv_linux()

    def getData(self):
        self.running = True
        self.running_df = True
        if self.data is not None:
            self.running = False
            self.running_df = False
            return self.data
        self.load()
        return self.data
    

class InputDataSheet(object):
    """Obter o dataframe de uma planilha Excel/CSV"""

    def __init__(self, *, fileSheet: FileSheet) -> None:
        self.fileSheet: FileSheet = fileSheet
        self.typeSheet: str = None
        self.currentText:str = ''
        self.currentProgress:int = 0
        self.running_df = False
        self.current_thread:Thread = None
        self.stop_event:Event = Event()

        if self.fileSheet.is_excel():
            self.typeSheet = '.xlsx'
            self.load:LoadSheet = LoadExcel(sheet=self.fileSheet)
        elif self.fileSheet.is_csv():
            self.typeSheet = '.csv'
            self.load = LoadCsv(sheet=self.fileSheet)
        else:
            raise Exception(f'Tipo de arquivo não suportado, use: .xlsx ou .csv')
        
    def stopCurrentThread(self):
        if self.current_thread is None:
            print(f'{__class__.__class__} nehuma Thread em execução')
            return
        self.stop_event.set()
        self.current_thread.join()
        self.current_thread = None
        self.running_df = False
        print(f'Thread principal encerrada!')

    def createCurrentThread(self):
        if self.current_thread is not None:
            print(f'{__class__.__name__} Thread principal já está em execução.')
            return
        if self.running_df == True:
            print(f'{__class__.__name__} outra operação já está em andamento.')
            return
        self.stop_event.clear()
        self.current_thread = Thread(target=self._execute)
        self.current_thread.start()
        self.running_df = True
        
    def getNumRows(self) -> int:
        return self.load.getNumRows()
        
    def _execute(self) -> None:
        self.load.getData()

    def load_values(self) -> bool:
        status = True
        self.createCurrentThread()
        while True:
            self.currentProgress = self.load.currentProgress
            self.currentText = self.load.currentText
            print(f'Progresso: {self.currentProgress:.2f}% - {self.currentText}', end='\r')
            if self.load.running_df == False:
                break
            if self.stop_event.is_set():
                status = False
                self.currentText = f'{__class__.__name__} Operação cancelada pelo usuário!'
                print(self.currentText)
                break
            time.sleep(1)
        print()
        self.running_df = False
        self.current_thread = None
        self.stop_event.clear()
        return status
    
    def dataframe(self) -> DataFrame | None:
        if not self.fileSheet.path.exists():
            std_util.e(f'O arquivo não existe: {self.fileSheet.absolute()}')
            return DataFrame()
        if not self.load_values():
            return DataFrame()
        return self.load.getData()


class FileText(IfaceFilePath):
    def __init__(self, filename: str):
        super().__init__(filename)
        self.__content_in_file: str = None

    @property
    def content(self) -> str:
        return self.__content_in_file
    
    @content.setter
    def content(self, new_line):
        if not isinstance(str, new_line):
            return
        self.__content_in_file = new_line

    def getLine(self) -> str:
        """
            Retorna uma string apartir do conteúdo do TXT
        """
        if self.content is None:
            self.content = self.path.read_text(encoding='utf8')
        return self.content

    def getLines(self, separator: str='\n') -> List[str]:
        """
            Retorna uma lista apartir do conteúdo do TXT
        """
        if self.getLine is None:
            return []
        return self.getLine().split(separator)
        
    def getDataText(self) -> DataText:
        return DataText(self.getLine(), separator='\n')
    
    def getDataList(self) -> DataList:
        if self.getLines() == []:
            return DataList(items=[], separator='\n')
        return DataList(items=self.getLines())
    
    def __write_lines_in_file(self, lines: List[str], open_mode: str='a+'):
        try:
            with open(self.absolute(), open_mode) as file:
                for line in lines:
                    file.write(f'{line}\n')
        except Exception as e:
            print(e)

    def __write_string_in_file(self, string: str, open_mode:str='a+'):
        try:
            with open(self.absolute(), open_mode) as file:
                file.write(f'{string}\n')
        except Exception as e:
            print(e)

    def writeText(self, text: str):
        self.__write_string_in_file(text, 'w')
        
    def appendText(self, text: str):
        self.__write_string_in_file(text, 'a+')

    def writeLines(self, lines:List[str]):
        self.__write_lines_in_file(lines, 'w')

    def appendLines(self, lines:List[str]):
        self.__write_lines_in_file(lines, 'a+')

class FileJson(IfaceFilePath):
    def __init__(self, filename: str):
        super().__init__(filename)
        self.__contentData:object = None

    def getContent(self) -> object:
        if self.__contentData is not None:
            return self.__contentData
        
        try:
            with open(self.absolute(), 'rt', encoding='utf8') as jfile:
                self.__contentData = json.load(jfile) # Texto do arquivo para dicionário.
        except Exception as e:
            print(__class__.__name__, e)
            return None
        else:
            return self.__contentData     

    def getTextJson(self) -> str:
        if self.getContent() is None:
            return None
        return json.dumps(self.getContent(), indent=4)
    
    def toDict(self) -> Dict[str, object]:
        if self.getTextJson() is None:
            return {}
        try:
            return json.loads(self.getTextJson())
        except:
            return {}
        
    def isEmpty(self) -> bool:
        if self.getContent() is None:
            return True
        return False
    
    def isIterable(self) -> bool:
        if self.getContent() is None:
            return False
        return is_iterable(self.getContent())
        
    def __write_data_in_file(self, dataJson: Dict[str, object], open_mode='a+'):
        """
            Escreve os dados JSON em self.absolute()
        """
        if not isinstance(dataJson, dict):
            print(f'{__class__.__name__} Erro: use dicionário ao invés de {dataJson}')
            return None
        
        try:
            with open(self.absolute(), open_mode, encoding='utf8') as file_json:
                json.dump(
                            dataJson, 
                            file_json, 
                            ensure_ascii=False, 
                            sort_keys=True, 
                            indent=4
                        )
        except Exception as e:
            print(f'{__class__.__name__}\n{e}')

    def write(self, *, dataJson: Dict[str, object]):
        """
            Escreve os dados JSON em self.absolute()
        """
        self.__write_data_in_file(dataJson, open_mode='w')

    def append(self, *, dataJson: Dict[str, object]):
        """
            Escreve os dados JSON em self.absolute()
        """
        self.__write_data_in_file(dataJson, open_mode='a+')


class FilePdf(File):
    def __init__(self, filename: str):
        super().__init__(filename)
        if not self.is_pdf():
            std_util.e(f'{__class__.__name__} {self.absolute()} não é um arquivo PDF')
            return
        self._pages: List[PageObject] = None
        self._current_reading:int = 0
        
    @property
    def pages(self) -> PageObject:
        return self._pages
    
    @pages.setter
    def pages(self, new:PageObject):
        self._pages = new

    def getCurrentReading(self) -> int:
        return self._current_reading

    def getPages(self) -> List[PageObject]:
        """Retorna uma lista de PageObject"""
        if self.pages is not None:
            return self._pages
        
        if not self.path.exists():
            std_util.e(f'[O ARQUIVO NÃO EXISTE]: {self.absolute()}')
            return []
        try:
            self.pages = PdfReader(self.absolute()).pages
        except Exception as e:
            print(f'{__class__.__name__} {e}')
            return []
        else:
            return self.pages
        
    def pagesList(self) -> List[str]:
        """
            Cada página do PDF será convertida em string, em seguida será retornado uma
        lista de strings.
        """
        pages_values: List[str] = []
        self._current_reading = 0
        maxNum:int = len(self.getPages())
        p: PageObject = None

        for p in self.getPages():
            self._current_reading += 1
            print(
                f'[LENDO PÁGINA]: {self._current_reading} de {maxNum} - {self.basename()}', end='\r'
            )
            current_text = p.extract_text()
            if (current_text is not None) and (current_text != ''):
                pages_values.append(p.extract_text())
            else:
                print(
                f'[LENDO PÁGINA]: {self._current_reading} de {maxNum} - {self.basename()} - ERRO', end='\n'
            )
        print()
        return pages_values
        
    def pagesDataText(self) -> List[DataText]:
        pages_list = self.pagesList()
        if pages_list == []:
            return
        
        items: List[DataText] = []
        for p in pages_list:
            items.append(
                    DataText(p, separator='\n')
                    )
        return items
    
    def getDataList(self) -> DataList:
        pages_list: List[str] = self.pagesList()
        if pages_list == []:
            return
        
        pages: List[str] = []
        for p in pages_list:
            pages.extend(p.split('\n'))
        return DataList(items=pages)
    

class FileImage(File):
    def __init__(self, filename: str):
        super().__init__(filename)
        if not self.is_image():
            std_util.e(f'{__class__.__name__} {self.absolute()} não é um arquivo de IMAGEM')
            return
 
          
class IfaceInputFiles(object):
    def __init__(self, *, inputDir: Directory, maxFiles:int = 200) -> None:
        self.inputDir: Directory = inputDir
        self.maxFiles:int = maxFiles
        if not isinstance(self.inputDir, Directory):
            print(f'{__class__.__name__} self.inputDir precisa ser Directory não {type(self.inputDir)}')
            sys.exit(1)

        self.__all_files:List[File] = []

    def update(self) -> List[File]:
        self.__all_files.clear()
        return self.getAllFiles()

    def setAllFiles(self, n:List[File]) -> None:
        if not isinstance(n, List[File]):
            return 
        self.__all_files = n

    def getAllFiles(self) -> List[File]:
        if self.__all_files != []:
            return self.__all_files

        filesPath: List[Path] = self.getFilesPath()
        for f in filesPath:
            self.__all_files.append(
                        File(os.path.abspath(f.absolute()))
                    )
        return self.__all_files

    def getFilesPath(self) -> List[Path]:
        """
            Retorna todos os arquivos com as extensões definidas
        """
        allFilesPath: List[Path] = [file for file in self.inputDir.path.rglob('*') if file.is_file()]
        if len(allFilesPath) > self.maxFiles:
            return  allFilesPath[0:self.maxFiles]
        return allFilesPath


class InputFiles(IfaceInputFiles):
    def __init__(self, *, inputDir: Directory, maxFiles: int = 200) -> None:
        super().__init__(inputDir=inputDir, maxFiles=maxFiles)

    def getFilesImage(self) -> List[FileImage]:
        if self.getAllFiles() == []:
            return []
        images: List[FileImage] = []
        for f in self.getAllFiles():
            if f.is_image():
                images.append(FileImage(f.absolute()))
        return images

    def getFilesPdf(self) -> List[FilePdf]:
        all_files: List[File] = self.getAllFiles()
        if all_files == []:
            return []
        file_pdf: List[FilePdf] = []
        for f in all_files:
            if f.is_pdf():
                file_pdf.append(FilePdf(f.absolute()))
        return file_pdf

    def getFilesSheet(self) -> List[FileSheet]:
        all_files: List[File] = self.getAllFiles()
        if all_files == []:
            return []
        files_sheet: List[FileSheet] = []
        for f in all_files:
            if f.is_excel():
                files_sheet.append(FileSheet(f.absolute()))
        return files_sheet

    def getFilesText(self) -> List[FileText]:
        if self.getAllFiles() == []:
            return []
        return [f for f in self.getAllFiles() if f.is_txt()]

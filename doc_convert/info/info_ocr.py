#!/usr/bin/env python3
#

import os
import shutil
from typing import (List, Dict)
from doc_convert import (
    File,
    Directory,
    FileJson,
    UserFileSystem,
    AppDirs,
    KERNEL_TYPE,
    download_file,
)


class UserPrefs(object):
    def __init__(self):
        self.prefs:Dict[str, str] = {}


class OcrAppDirs(AppDirs):
    """
        Classe para definir e manipular diretórios e arquivos padrão desse aplicativo.
    """
    def __init__(self, *, appname: str, ufs: UserFileSystem = UserFileSystem()) -> None:
        super().__init__(appname=appname, ufs=ufs)
        self.ufs:UserFileSystem = ufs
        self._path_bin_tesseract: File = None # Executável tesseract
        self._path_bin_ocrmypdf:File = None # Executável ocrmypdf
        self._path_file_json:FileJson = None

    @property
    def path_bin_tesseract(self) -> File:
        return self._path_bin_tesseract
    
    @path_bin_tesseract.setter
    def path_bin_tesseract(self, value:File):
        if isinstance(value, str):
            value = File(value)
        if not isinstance(value, File):
            print(f'{__class__.__name__} Erro _path_tesseract precisa ser do tipo File() naõ {type(value)}')
        self._path_bin_tesseract = value

    @property
    def path_bin_ocrmypdf(self) -> File:
        return self._path_bin_ocrmypdf

    @path_bin_ocrmypdf.setter
    def path_bin_ocrmypdf(self, value:File):
        if isinstance(value, str):
            value = File(value)
        if not isinstance(value, File):
            print(f'{__class__.__name__} Erro _path_ocrmypdf precisa ser do tipo File() naõ {type(value)}')
        self._path_bin_tesseract = value

    def dirOutputFiles(self) -> Directory:
        """
            Diretório raiz para exportar os arquivos processados por esse programa.
        O padrão inicial é um diretório dentro da pasta Downloads, mas pode ser alterado pelo usuário.
        """
        d = self.ufs.dir_downloads.concat_dir(self.appname)
        d.mkdir()
        return d

    def dirCache(self) -> Directory:
        """
            Diretório para guardar o cache local
        """
        d = self.dirOutputFiles().concat_dir('cache')
        d.mkdir()
        return d

    def dirConfig(self) -> Directory:
        d = self.dirOutputFiles().concat_dir('config')
        d.mkdir()
        return d

    def fileJson(self) -> FileJson:
        if self._path_file_json is not None:
            return self._path_file_json
        return FileJson(self.dirConfig().joinFile('config.json').absolute())
    
    def _tesseract_system(self) -> File | None:
        """
            Verificar se o tesseract está instalado, se estiver
        retorna um objeto File(), se não retorna None.
        """
        name: str = 'tesseract'
        if KERNEL_TYPE == 'Windows':
            name += '.exe'
        if shutil.which(name) is not None:
            return File(shutil.which(name))
        return  None
    
    def fileTesseract(self) -> File:
        """
            Se o tesseract estiver instalado no sistema, será retornado um objeto
        File() apantando para o arquivo executável, se não será retornado um objeto
        File() apontando para um arquivo de cache local para ser baixado nesse caminho
        posteriormente.
        """
        # Verificar se o executável foi definido externamente.
        if self._path_bin_tesseract is not None:
            return self._path_bin_tesseract
        
        # Verificar se o executável tesseract está instalado no sistema.
        out = self._tesseract_system()
        if out is not None:
            return out # Está instalado.
        # Apontar para um destino padrão.
        name:str = 'tesseract'
        if KERNEL_TYPE == 'Windows':
            name += '.exe'
        return self.dirCache().joinFile(name)

    def _ocrmypdf_system(self) -> File | None:
        name = 'ocrmypdf'
        if KERNEL_TYPE == 'Windows':
            name += '.exe'
        if shutil.which(name) is not None:
            return  File(shutil.which(name))
        return None

    def fileOcrmypdf(self) -> File | None:
        if self.path_bin_ocrmypdf is not None:
            return self.path_bin_ocrmypdf

        out = self._ocrmypdf_system()
        if out is not None:
            return out
        
        n = 'ocrmypdf.exe' if KERNEL_TYPE == 'Windows' else 'ocrmypdf' 
        return self.dirCache().joinFile(n)

    def default_prefs(self) -> UserPrefs:
        p = UserPrefs()
        p.prefs['path_tesseract'] = self.fileTesseract().absolute()
        p.prefs['path_ocrmypdf'] = self.fileOcrmypdf().absolute()
        p.prefs['dir_output_files'] = self.dirOutputFiles().absolute()
        p.prefs['dir_cache'] = self.dirCache().absolute()
        p.prefs['dir_config'] = self.dirConfig().absolute()
        p.prefs['file_json'] = self.fileJson().absolute()
        p.prefs['initial_dir'] = self.ufs.dir_downloads.absolute()
        return p

class PackageTesseractPortableWin(object):
    """Insalar o Tesseract portable"""
    def __init__(self, *, ocrAppdirs:OcrAppDirs) -> None:
        self.ocrAppDirs: OcrAppDirs = ocrAppdirs
        # https://github.com/zstrathe/tesseract_portable_windows
        self.unpackDir:Directory = Directory(self.ocrAppDirs.dirCache().concat_dir('unpack-files').absolute())
        self.pkgTesseract: File = self.unpackDir.joinFile('tesseract_portable.zip')
        self.url = 'https://github.com/zstrathe/tesseract_portable_windows/archive/refs/heads/main.zip'

    def download_tesseract(self):
        download_file(url=self.url, file_path=self.pkgTesseract)

    def dirOpt(self):
        return self.ocrAppDirs.dirOpt()
    
    def dirTesseractPortable(self) -> Directory:
        d = self.ocrAppDirs.dirOutputFiles().concat_dir('Tesseract-Portable')
        d.mkdir()
        return d
    
    def fileTesseractPortable(self) -> File:
        return self.dirTesseractPortable().joinFile('tesseract.exe')

    def unpack_tesseract(self):
        if not self.unpackDir.path.exists():
            self.unpackDir.mkdir()
        os.chdir(self.unpackDir.absolute())
        shutil.unpack_archive(self.pkgTesseract.absolute())

        for i in os.listdir('.'):
            if 'tesseract_portable_windows' in i:
                shutil.move(i, 'tesseract')
                break

    def _run(self):
        self.download_tesseract()
        self.unpack_tesseract()
        d = self.unpackDir.concat_dir('tesseract').absolute()
        os.chdir(d)
       
        for item in os.listdir('.'):
            origin = os.path.join(d, item)
            dest = os.path.join(
                                self.dirTesseractPortable().absolute(),
                                item,
                            )
            print(f'Copiando: {item}')
            if os.path.isdir(origin):
                shutil.copytree(origin, dest)
            else:
                shutil.copy2(origin, dest)
        #shutil.rmtree(self.unpackDir.absolute())
        print('OK')

    def install(self):
        if not self.fileTesseractPortable().path.exists():
            self._run()
        
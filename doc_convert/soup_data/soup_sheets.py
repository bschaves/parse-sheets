#!/usr/bin/env python3
#
#
#
# https://pythonspot.com/read-excel-with-pandas/
# https://www.cybrosys.com/blog/how-to-convert-csv-to-excel-using-pandas-in-python
# https://www.covildodev.com.br/artigo/remover-item-dicionario-python
# https://www.analisededados.blog.br/2020/04/importando-excel-no-python-pandas.html
# https://pandas.pydata.org/docs/reference/api/pandas.Series.str.cat.html 
#

import sys

try:
    import pandas
    from pandas.core.series import (Series)
    from pandas import (
        DataFrame,
    )

    from typing import (List, Dict, Any)
except Exception as e:
    print(e)
    sys.exit(1)

from doc_convert.utils.util import (
    Directory,
    File,
    std_util,
    KERNEL_TYPE,
)

from doc_convert.common.text_util import remove_chars
from doc_convert.common.sheets import (
                                        FormatData,
                                        save_dataframe,
                                        )



class FindExportTextUniqFile(object):
    """Filtra textos em uma planilha e exporta para um arquivo"""
    def __init__(self, *, parse: FormatData, columnName:str, texts: List[str], outFile: File) -> None:
        self.parse: FormatData = parse
        self.texts: List[str] = texts
        self.outFile: File = outFile
        self.columnName:str = columnName

    def execute(self):
        maxNum = len(self.texts)
        df_elements: List[DataFrame] = []

        for n, text in enumerate(self.texts):
            print(f'Filtrando {n+1} de {maxNum} - {text}')
            df: DataFrame = self.parse.getElements(iqual=True, column=self.columnName, text=text)
            if df is not None:
                df_elements.append(df)

        all_df: DataFrame = pandas.concat(df_elements)
        save_dataframe(outFile=self.outFile, data=all_df)
        std_util.print_line()


class FindExportTextMultFiles(object):
    def __init__(self, *, parse: FormatData, columnName:str, texts: List[str], outDir: Directory) -> None:
        self.parse: FormatData = parse
        self.texts: List[str] = texts
        self.outDir: Directory = outDir
        self.columnName:str = columnName

    def _getFileFromName(self, name) -> File:
        name = remove_chars(name)
        return self.outDir.joinFile(f'{name}.xlsx')
    
    def getDataFrames(self) -> List[DataFrame]:
        pass
    
    def execute(self) -> None: 
        maxNum = len(self.texts)

        for n, text in enumerate(self.texts):
            print(f'Filtrando {n+1} de {maxNum} - {text}', end='\r')
            df: DataFrame = self.parse.getElements(column=self.columnName, text=text, iqual=True)
            if df is not None:
                out_file = self._getFileFromName(text)
                save_dataframe(data=df, outFile=out_file)
        print()
        std_util.print_line()
            

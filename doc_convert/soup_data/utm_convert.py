#!/usr/bin/env python3
#
from __future__ import annotations
from typing import (List, Tuple, Dict, LiteralString)
import utm
import folium
import os
import pandas
from pandas import DataFrame
from doc_convert import KERNEL_TYPE, Directory, File


class LatLongPoint(object):
    def __init__(self, lat:float, long:float, name='ROTA'):
        self.lat:float = lat
        self.long:float = long
        self.name:str = name

    def to_tuple(self) -> Tuple[float, float]:
        return (self.lat, self.long)
    
    def to_utm_point(self, *, zone:int, latter:str) -> UtmPoint | None:
        try:
            _east=utm.from_latlon(self.lat, self.long)[0] 
            _north=utm.from_latlon(self.lat, self.long)[1]
        except Exception as e:
            print(f'\n{__class__.__name__}\n{e}')
            return None
        else:
            return UtmPoint(
                        east=_east, 
                        north=_north, 
                        zone=zone, 
                        letter=latter
                )


class UtmPoint(object):
    """
        Objeto que representa uma coordenada UTM.
    """
    def __init__(self, *, east:object, north:object, zone:int=20, letter:str='K'):
        """
        easting = utm_coords[0] 
        northing = utm_coords[1] 
        zone_number = utm_coords[2] 
        zone_letter = utm_coords[3
        """

        if len(str(east)) > 6:
            east = str(east)[0:6]
        if len(str(north)) > 7:
            north=str(north)[0:7]

        self.east:object = float(east)
        self.north:object = float(north)
        self.zone:int = int(zone)
        self.letter:str = letter # hemisphere

        if (self.north < 0) or (self.north > 10000000):
            raise ValueError(f'{__class__.__name__} o valor de north deve estar entre 0 e 10000000. Recebido: {self.north}')
        if (self.east < 100000) or (self.east > 999999):
            raise ValueError(f'{__class__.__name__} o valor de east deve estar entre 100 e 999999. Recebido: {self.east}')

    def to_latlong(self) -> LatLongPoint | None:
        # Conversão de UTM para Latitude/Longitude
        try:
            lat, long = utm.to_latlon(self.east, self.north, self.zone, self.letter)
        except Exception as e:
            print(f'{__class__.__name__} -> {e}')
            return None
        else:
            return LatLongPoint(lat, long)

#======================================================================#
class ConvertPoints(object):
    def __init__(self):
        pass

    def from_lat_lon_points(self, *, points:List[LatLongPoint], zone:int, latter:str) -> List[UtmPoint]:
        utm_points = []
        for latlong in points:
            p = latlong.to_utm_point(zone=zone, latter=latter)
            if p is not None:
                utm_points.append(p)
        return utm_points

    def from_utm_points(self, points:List[UtmPoint]) -> List[LatLongPoint]:
        latlon_points = []
        for utm_point in points:
            p = utm_point.to_latlong()
            if p is not None:
                latlon_points.append(p)
        return latlon_points

    def from_data_utm(
        self, *,
        data:DataFrame, 
        column_east:str, 
        column_north:str, 
        zone:int=20, 
        letter:str='K',) -> DataFrame:
        """
            Recebe um DataFrame contendo coordenadas UTM e
        retorna um novo DataFrame() inserindo as coordenadas
        Latitude/Longitude.
        """
        #
        # Apagar linhas vazias nas colunas de coordenadas.
        data = data.dropna(subset=[column_east, column_north])
        data = data.astype('str')
        #
        index_esting = data.columns.tolist().index(column_east)
        index_nort = data.columns.tolist().index(column_north)
        utm_points = []
        for value in data.values:
            #
            #
            try:
                current = UtmPoint(
                        east=value[index_esting], 
                        north=value[index_nort], 
                        zone=zone, 
                        letter=letter,
                    )
            except Exception as e:
                print(f'[PULANDO] {value[index_esting]} - {e}')
                utm_points.append(None)
            else:
                print(f'Adicinando: {value[index_esting]}')
                utm_points.append(current)
        #
        if len(utm_points) < 1:
            return None
        #
        values_lat = []
        values_lon = []
        u:UtmPoint = None
        for u in utm_points:
            if u is None:
                values_lat.append(None)
                values_lon.append(None)
                continue
            p = u.to_latlong()
            if p is not None:
                values_lat.append(p.lat)
                values_lon.append(p.long)
        if (len(values_lat) < 1) or (len(values_lon) < 1):
            return None
        
        data['LATITUDE'] = values_lat
        data['LONGITUDE'] = values_lon
        #
        #
        data = data[data['LATITUDE'] != '']  # Remover strings vazias
        data = data.dropna(subset=['LATITUDE'])  # Remover valores nulos 
        return data

#======================================================================#
class RouteLatLon(object):
    """
        Representação de uma rota com vários pares de coordenada LATITUDE/LONGITUDE.
    """
    def __init__(self, points:List[LatLongPoint]):
        self.points:List[LatLongPoint] = points
        self.num_points:int = len(self.points)

    def add_point(self, p:LatLongPoint):
        if not isinstance(p, LatLongPoint):
            print(f'{__class__.__name__}\n{p} não é válido.')
            return
        self.points.append(p)

    def first(self) -> LatLongPoint:
        return self.points[0]

    def last(self) -> LatLongPoint:
        return self.points[-1]

    def is_empty(self) -> bool:
        return True if self.num_points == 0 else False
    
    def get_point(self, p:int) -> LatLongPoint:
        return self.points[p]

#======================================================================#
class CreateMap(object):
    """
        Recebe um obejto RouteLatLon e pode 
    gerar um mapa ou um arquivo HTML de mapa com os dados do objeto RouteLatLon.
    """
    def __init__(self, route:RouteLatLon):
        self.route:RouteLatLon = route
        
    def is_empty(self) -> bool:
        return self.route.is_empty()
    
    def get_map_from_route(self) -> folium.Map | None:
        """
            Gera um objeto mapa de pontos.
        """
        if not isinstance(self.route, RouteLatLon):
            print(f'{__class__.__name__}\n{self.route} não é válido.')
            return None
        if self.route.is_empty():
            return None
        
        map:folium.Map = folium.Map(
                location=[
                    self.route.first().lat, 
                    self.route.first().long,
                ], 
                zoom_start=15
            )

        # Adiciona um marcador no ponto
        if self.route.num_points > 1:
            
            for num in range(1, self.route.num_points):
                folium.Marker(
                    [self.route.get_point(num).lat, self.route.get_point(num).long], 
                    popup=f"Lat: {self.route.get_point(num).lat} | Lon: {self.route.get_point(num).long} | {self.route.get_point(num).name}"
                ).add_to(map)
        else:
            folium.Marker(
                [self.route.first().lat, self.route.first().long], 
                popup=f"Lat: {self.route.first().lat} | Lon: {self.route.first().long} | {self.route.first().name}"
            ).add_to(map)
        #
        return map

    def to_html(self, file:str) -> None:
        """
            Salva um objeto mapa de pontos em um arquivo HTML.
        """
        if not isinstance(file, str):
            raise ValueError(f'{__class__.__name__}\n{file} não é um arquivo válido')
        #
        # Cria um mapa centrado no ponto convertido
        _map:folium.Map = self.get_map_from_route()
        # Salva o mapa em um arquivo HTML
        _map.save(file)
        print(f'Mapa salvo em {file}')

#======================================================================#

def get_utm(*, lat:float, lon:float) -> UtmPoint:
    """
        Recebe um par de coordenadas Latitude/Longitude e retorna um objeto UtmPoint() com as coordenadas UTM.
    """
    # Tuple[float, float, int, object]
    _utm = utm.from_latlon(lat, lon)
    return UtmPoint(
                east=_utm[0], 
                north=_utm[1], 
                zone=_utm[2], 
                letter=_utm[3]
            )

def get_data_latlong(
        file:str, 
        col_lat='vlr_coordenada_latitude', 
        col_long='vlr_coordenada_longitude'
        ) -> DataFrame:
    
    df = pandas.read_excel(file)
    df = df[[col_lat, col_long]]
    return df


#======================================================================#
def create_map(rota:Rota, outfile:str):
    if rota.is_empty():
        raise ValueError('Rota Vazia')
    
    # Cria um mapa centrado no ponto convertido
    map = folium.Map(
            location=[
                rota.first()[0], 
                rota.first()[1]
            ], 
            zoom_start=12
        )

    # Adiciona um marcador no ponto
    # folium.Marker([lat, lon], popup=f"Lat: {lat}, Lon: {lon}").add_to(mapa)
    if rota.max_pontos > 1:
        for num in range(1, rota.max_pontos):
            folium.Marker(
                [rota.get_point(num)[0], rota.get_point(num)[1]], 
                popup=f"Lat: {rota.get_point(num)[0]} | Lon: {rota.get_point(num)[1]}"
                ).add_to(map)
    else:
        folium.Marker(
            [rota.first()[0], rota.first()[1]], 
            popup=f"Lat: {rota.first()[0]} | Lon: {rota.first()[1]}"
            ).add_to(map)


    # Salva o mapa em um arquivo HTML
    map.save(outfile)
    print(f'Mapa salvo em {outfile}')

#======================================================================#

def create_map_from_df(data:DataFrame, outfile:str):
    values = list(data.itertuples(index=False, name=None))
    rota = Rota(values)
    create_map(rota, outfile)
    return
    if KERNEL_TYPE == 'Linux':
        os.system(f'xdg-open {outfile}')
    elif KERNEL_TYPE == 'Windows':
        # chrome_path = r"C:\Program Files\Google\Chrome\Application\chrome.exe"
        # subprocess.run([chrome_path, outfile])
        os.startfile(outfile)

#======================================================================#

def run_teste():
    pass

    

#!/usr/bin/env python3
#

from setuptools import setup, find_packages

setup(
    name="document_convert",
    version="1.2",
    author="Bruno",
    description="Converte documentos digitalizados para texto.",
    packages=find_packages(
        where='.', 
        include=[
            'document_convert', 
            'document_convert.*'
        ]
    ),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: MIT License",
        "Operating System :: Windows :: Linux",
    ],
    python_requires='>=3.7',
)

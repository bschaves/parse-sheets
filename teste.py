#!/usr/bin/env python3
#
import folium
from typing import Tuple,List

class MapaRotas:
    def __init__(self, localizacao_inicial:Tuple[float, float], zoom=13, titulo="Mapa de Rotas"):
        """
        Inicializa o mapa.
        :param localizacao_inicial: Coordenadas [latitude, longitude] para centralizar o mapa.
        :param zoom: Nível de zoom inicial.
        :param titulo: Título do mapa.
        """
        self.mapa = folium.Map(location=localizacao_inicial, zoom_start=zoom)
        self.titulo = titulo

    def adicionar_rota(self, coordenadas:List[Tuple[float, float]], cor, label):
        """
        Adiciona uma rota ao mapa com marcadores e uma linha conectando os pontos.
        :param coordenadas: Lista de tuplas (latitude, longitude).
        :param cor: Cor da linha e dos marcadores.
        :param label: Rótulo para identificar a rota.
        """
        # Adicionar linha conectando os pontos
        folium.PolyLine(coordenadas, color=cor, weight=5, opacity=0.7, tooltip=label).add_to(self.mapa)
        
        # Adicionar marcadores para cada ponto
        for ponto in coordenadas:
            folium.CircleMarker(
                location=ponto,
                radius=5,
                color=cor,
                fill=True,
                fill_color=cor,
                fill_opacity=0.9,
                tooltip=f"{label}: {ponto}"
            ).add_to(self.mapa)

    def salvar(self, nome_arquivo):
        """
        Salva o mapa em um arquivo HTML.
        :param nome_arquivo: Nome do arquivo de saída.
        """
        self.mapa.save(nome_arquivo)

# Coordenadas reais de exemplo
rota1_coords = [
    (-23.550520, -46.633308),  # São Paulo
    (-23.559616, -46.622200),
    (-23.566499, -46.606326)
]
rota2_coords = [
    (-23.550520, -46.633308),  # São Paulo
    (-23.562000, -46.641700),
    (-23.570501, -46.655678)
]

# Criar o mapa centralizado na primeira coordenada
mapa = MapaRotas(localizacao_inicial=rota1_coords[0], zoom=14, titulo="Rotas de Exemplo")

# Adicionar as rotas
mapa.adicionar_rota(rota1_coords, "blue", "Rota 1")
mapa.adicionar_rota(rota2_coords, "red", "Rota 2")

# Salvar o mapa em um arquivo HTML
mapa.salvar("mapa_rotas.html")
print("Mapa salvo como 'mapa_rotas.html'. Abra o arquivo em um navegador para visualizar.")

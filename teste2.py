#!/usr/bin/env python3
#
import folium

# Dados das rotas
rota1 = [(51.5, -0.1), (51.52, -0.1), (51.54, -0.1)]  # Coordenadas da rota 1
rota2 = [(51.5, -0.12), (51.52, -0.12), (51.54, -0.12)]  # Coordenadas da rota 2

# Criar o mapa centralizado na primeira coordenada
mapa = folium.Map(location=[51.5, -0.1], zoom_start=13)

# Adicionar os pontos da rota 1 ao mapa
for ponto in rota1:
    folium.CircleMarker(location=ponto, radius=5, color='blue', fill=True, fill_color='blue', popup="Rota 1").add_to(mapa)

# Adicionar os pontos da rota 2 ao mapa
for ponto in rota2:
    folium.CircleMarker(location=ponto, radius=5, color='red', fill=True, fill_color='red', popup="Rota 2").add_to(mapa)

# Exibir o mapa
mapa.save("mapa_rotas.html")

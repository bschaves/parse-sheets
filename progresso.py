#!/usr/bin/env python3
#

import sys
from typing import List, Tuple
import tkinter as tk
from tkinter import ttk
from threading import Thread
import pandas as pd
from pandas import DataFrame
from openpyxl import load_workbook, Workbook
from openpyxl.worksheet._read_only import ReadOnlyWorksheet
from tqdm import tqdm

class ProgressBarHandler(object):
    def __init__(self, file:str, chunk_size:int, progress_bar, status_label):
        self.file:str = file
        self.chunk_size:int = chunk_size
        self.progress_bar = progress_bar
        self.status_label = status_label

        self.current_progress:float = 0 # 0 -> 1
        self.current_percent:float = 0 # 0% -> 100%
        self.__read_only_sheet:ReadOnlyWorksheet = None

    def getReadSheet(self) -> ReadOnlyWorksheet:
        if self.__read_only_sheet is not None:
            return self.__read_only_sheet
        
        workbook:Workbook = load_workbook(self.file, read_only=True)
        self.__read_only_sheet = workbook.active
        return self.__read_only_sheet
    
    def getMaxRows(self) -> int:
        return self.getReadSheet().max_row

    def ler_excel_com_progresso(self):
        sheet:ReadOnlyWorksheet = self.getReadSheet()
        total_linhas:int = self.getMaxRows()
        
        lista_chunks:List[DataFrame] = []
        data:List[tuple] = []

        with tqdm(total=total_linhas, desc='Lendo Excel', unit='linha') as pbar:
            for i, row in enumerate(sheet.iter_rows(values_only=True)):
                data.append(row)
                if (i + 1) % self.chunk_size == 0:
                    # Ainda está em 0%
                    df_chunk:DataFrame = pd.DataFrame(data)
                    lista_chunks.append(df_chunk)
                    data = []
                pbar.update(1)
                self.current_progress = (i+1) / total_linhas
                #progress = (i + 1) / total_linhas * 100
                self.update_progress()
        
            if data:
                df_chunk = pd.DataFrame(data)
                lista_chunks.append(df_chunk)
        
        df_completo = pd.concat(lista_chunks, ignore_index=True)
        return df_completo

    def update_progress(self):
        self.current_percent = self.current_percent * 100
        self.progress_bar["value"] = self.current_percent
        self.status_label.config(text=f"Progresso: {self.current_percent:.2f}%")


class ExcelReaderGUI:
    def __init__(self, master):
        self.master = master
        self.master.title("Leitura de Excel com Progresso")
        
        self.progress_bar = ttk.Progressbar(master, orient="horizontal", length=300, mode="determinate")
        self.progress_bar.grid(column=0, row=0, padx=10, pady=10)
        
        self.status_label = ttk.Label(master, text="Progresso: 0%")
        self.status_label.grid(column=0, row=1, padx=10, pady=10)
        
        self.botao_iniciar = ttk.Button(master, text="Iniciar Leitura", command=self.iniciar_leitura)
        self.botao_iniciar.grid(column=0, row=2, padx=10, pady=10)
        
        self.caminho_do_arquivo = '/home/bruno/Downloads/2024-09-ANALITICO.xlsx'

    def iniciar_leitura(self):
        handler = ProgressBarHandler(self.caminho_do_arquivo, 1000, self.progress_bar, self.status_label)
        thread = Thread(target=handler.ler_excel_com_progresso)
        thread.start()


if __name__ == "__main__":
    root = tk.Tk()
    app = ExcelReaderGUI(root)
    root.mainloop()
